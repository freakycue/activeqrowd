'use strict';

var express = require('express'),
  connect = require('connect'),
  MongoStore = require('connect-to-mongo')(connect),
  passport = require('passport'),
  config = require('./config')(),
  helpers = require('./utils/helpers'),
  hbs = require('express-hbs'),
  mongoose = require('mongoose');

mongoose.connect(config.db);
// init models
require('./models');

var app = express(),
  hbsEngine = hbs.express3({
    partialsDir: __dirname + '/views/partials',
    layoutsDir: __dirname + '/views/layout',
    defaultLayout: __dirname + '/views/layout/default.hbs'
  });

require('./authStrategies');

// res.redirect patch
require('./utils/smartRedirectPatch');
require('express-dynamic-helpers-patch')(app);
app.dynamicHelpers(require('./views/dynamicHelpers'));

app.set('port', process.env.PORT || 3000);
// setup handlebars
app.engine('hbs', hbsEngine);
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');
// helpers
require('handlepers');
require('./views/helpers')(hbs);

app.use(connect.logger());

app.use(require('body-parser')());
app.use(express.static(__dirname + '/public/'));

app.use(require('cookie-parser')(config.secret));
app.use(require('express-session')({
  store: new MongoStore({
    url: config.db
  }),
  secret: config.secret,
  cookie: {
    //30 days
    maxAge: 30 * 24 * 60 * 60 * 1000
  }
}));
app.use(passport.initialize());
app.use(passport.session());
// authenticated cookie middleware
app.use(require('./middleware/authCookie'));

// editable content middleware
app.get(new RegExp('^/(' + helpers.editablePages.join('|') + ')?$'),
  require('./middleware/getEditableContent'));

// initalize streams
var tweetStream = require('./streams/tweet');

// init routers
app.use(require('./routers'));
// auth router
app.use(require('./routers/auth'));
// system router
app.use('/system', require('./routers/system'));
// admin router
app.use('/admin', require('./routers/admin'));
// 404 page
app.use(require('./middleware/notFound'));
// error handler
app.use(require('./middleware/errorHandler'));

var server = require('http').createServer(app);
// init web sockets
require('./socket')(app, server, tweetStream);

server.listen(config.port);
console.log('Server started at port ' + config.port + ' with ' +
  (process.env.NODE_ENV || 'development') + ' config');
