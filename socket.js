'use strict';

var socketIO = require('socket.io');

module.exports = function(app, server, stream) {
  var io = socketIO.listen(server);
  io.configure(function() {
    io.set('log level', 1);
  });
  io.set('browser client minification', true);
  io.set('browser client etag', true);
  io.set('browser client gzip', true);
  io.set('transports', [
    'websocket',
    'htmlfile',
    'xhr-polling',
    'jsonp-polling'
  ]);

  stream.on('tweet', function(tweet) {
    app.render('tweet/item', {
      tweet: tweet
    }, function(err, html) {
      if (err) return console.log('[err]', err, err.stack);

      tweet.view = html;
      io.sockets.emit('tweet', tweet);
    });
  });
};
