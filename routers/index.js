'use strict';

var express = require('express'),
  ensureLogin = require('../middleware/ensureLogin'),
  ensureProfileComplete = require('../middleware/ensureProfileComplete');

var router = module.exports = express.Router();

router.all(/^\/(tweets|recipients)/, ensureLogin,
  ensureProfileComplete);
router.all(/^\/campaigns\/search/, ensureLogin,
  ensureProfileComplete);
router.all(/^\/orders/, ensureLogin,
  ensureProfileComplete);
router.all(/^\/profile\/(my|complete)/, ensureLogin);
router.all('/activists/:handle([A-Za-z0-9_]{1,15})/edit',
  ensureLogin,
  ensureProfileComplete, require('../middleware/protectProfile'));

require('./campaign')(router);
require('./main')(router);
require('./order')(router);
require('./activist')(router);
require('./recipient')(router);
require('./static')(router);
require('./tweet')(router);
