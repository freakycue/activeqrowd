'use strict';

var helpers = require('../utils/helpers'),
  mongoose = require('mongoose'),
  config = require('../config')();

var Static = mongoose.model('Static');

module.exports = function(router) {
  router.param('page', function(req, res, next, page) {
    Static.findOne({key: page}, function(err, pageContent) {
      if (err) return next(err);

      req.pageStaticContent = pageContent;
      next();
    });
  });

  router.get('/:page(' + helpers.staticPages.join('|') + ')',
  function(req, res) {
    res.render('static/index');
  });

  router.get('/robots.txt', function(req, res) {
    res.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    res.end([
      'User-agent: *',
      'Disallow: /auth/',
      'Disallow: /admin/',
      'Disallow: /tweets/',
      'Sitemap: ' + config.basePath + 'sitemap.xml',
      'Host: activeqrowd.com'
    ].join('\n'));
  });
};
