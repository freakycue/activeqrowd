'use strict';

var Steppy = require('twostep').Steppy,
  mongoose = require('mongoose'),
  twitter = require('../services/twitter');

var Campaign = mongoose.model('Campaign');

module.exports = function(router) {

  router.param('campaign', require('../middleware/campaignFallback'));

  // results per page
  var limit = 15;
  router.get('/campaigns', function(req, res, next) {
    var page = Number(req.query.p || 1);

    Steppy(
      function() {
        var conditions = {isHidden: false};
        if (req.query.q) {
          conditions.name = {$regex: req.query.q, $options: 'i'};
        }

        Campaign.find(conditions, {
          'relatedCampaigns': 0,
          'mentions': 0
        }, {
          limit: limit,
          skip: limit * (page - 1)
        }).sort({'counters.tweets.count': -1}).exec(this.slot());
        Campaign.count(conditions, this.slot());
      },
      function(err, campaigns, count) {
        res.render('campaign/list', {
          campaigns: campaigns,
          q: req.query.q,
          pagination: {
            page: page,
            pageCount: Math.ceil(count / limit)
          }
        });
      },
      next
    );
  });

  router.post('/campaigns/search', function(req, res, next) {
    Steppy(
      function() {
        var query = req.param('query');

        if (query) {
          Campaign.find({
            name: {$regex: '^' + query, $options: 'i'}
          }, {name: 1}, {limit: 15}).sort({name: 1}).exec(this.slot());
        } else this.pass([]);
      },
      function(err, campaigns) {
        res.json(campaigns);
      },
      next
    );
  });

  router.get('/campaigns/:campaign(\\w+)', function(req, res, next) {
    var campaign = req.campaign;
    Steppy(
      function() {
        twitter.search('#' + campaign.name, {
          limit: 100
        }, this.slot());
      },
      function(err, tweets) {
        res.render('campaign/view', {
          campaign: campaign,
          tweets: tweets
        });
      },
      next
    );
  });
};
