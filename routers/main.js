'use strict';

var Steppy = require('twostep').Steppy,
  _ = require('underscore'),
  mongoose = require('mongoose'),
  twitter = require('../services/twitter');

var Campaign = mongoose.model('Campaign');

module.exports = function(router) {
  router.get('/', function(req, res, next) {
    Steppy(
      function() {
        // get last tweet
        twitter.getLastTweet(this.slot());

        // get 3 top campaigns
        Campaign.find({
          isHidden: false
        }, {
          name: 1
        }, {limit: 3})
        .sort({'counters.tweets.count': -1}).exec(this.slot());
      },
      function(err, lastTweet, campaigns) {
        this.pass(lastTweet);
        this.pass(campaigns);

        if (campaigns.length) {
          var group = this.makeGroup();
          campaigns.forEach(function(campaign) {
            twitter.searchByCampaign(campaign.name, {count: 20}, group.slot());
          });
        }
      },
      function(err, lastTweet, campaigns, campaignsTweets) {
        res.render('index', {
          campaign: req.query.campaign,
          campaigns: _(campaigns).filter(function(campaign, index) {
            campaign.tweets = campaignsTweets[index];
            return campaign.tweets.length;
          }),
          lastTweet: lastTweet
        });
      },
      next
    );
  });

  router.get('/activity', function(req, res, next) {
    twitter.search(req.query.q, function(err, tweets) {
      if (err) return next(err);
      res.render('activity', {
        tweets: tweets,
        q: req.query.q
      });
    });
  });
};
