'use strict';

var Steppy = require('twostep').Steppy,
  mongoose = require('mongoose'),
  searchHelper = require('../../utils/helpers').search;

var Recipient = mongoose.model('Recipient');

module.exports = function(router) {
  // results per page
  var limit = 30;
  var searchRecipients = searchHelper({
    model: Recipient,
    limit: limit
  });

  router.param('recipientId', function(req, res, next, recipientId) {
    Recipient.findById(recipientId, function(err, recipient) {
      if (err) return next(err);

      req.recipient = recipient;
      next();
    });
  });

  router.get('/recipients', function(req, res, next) {
    var page = Number(req.query.p || 1);

    Steppy(
      function() {
        searchRecipients(req.query.q, {
          page: page,
          sort: {createDate: -1}
        }, this.slot());
      },
      function(err, data) {
        res.render('admin/recipient/list', {
          recipients: data.results,
          q: req.query.q,
          pagination: {
            page: page,
            pageCount: Math.ceil(data.totalCount / limit)
          }
        });
      },
      next
    );
  });

  router.post('/recipients/add', function(req, res, next) {
    Steppy(
      function() {
        var recipient = new Recipient(req.body);
        recipient.save(this.slot());
      },
      function() {
        res.redirect('/recipients');
      },
      next
    );
  });

  router.route('/recipients/:recipientId([a-f0-9]+)')
  .get(function(req, res) {
    res.render('admin/recipient/form', {
      recipient: req.recipient
    });
  })
  .post(function(req, res, next) {
    var recipient = req.recipient;
    Steppy(
      function() {
        recipient.set(req.body);
        recipient.save(this.slot());
      },
      function() {
        res.redirect('/recipients');
      },
      next
    );
  });

  router.get('/recipients/:recipientId([a-f0-9]+)/remove',
  function(req, res, next) {
    Steppy(
      function() {
        req.recipient.remove(this.slot());
      },
      function() {
        res.redirect('/recipients');
      },
      next
    );
  });
};
