'use strict';

var mongoose = require('mongoose'),
  _ = require('underscore'),
  Steppy = require('twostep').Steppy,
  searchHelper = require('../../utils/helpers').search;

var Campaign = mongoose.model('Campaign');

module.exports = function(router) {
  // results per page
  var limit = 30;
  var searchCampaigns = searchHelper({
    model: Campaign,
    limit: limit
  });

  router.param('campaign', require('../../middleware/campaignFallback'));

  router.get('/campaigns', function(req, res, next) {
    var page = Number(req.query.p || 1);
    Steppy(
      function() {
        var conditions = {};
        if (req.query.isHidden) {
          conditions.isHidden = true;
        }
        searchCampaigns(req.query.q, {
          page: page,
          sort: {createDate: -1},
          conditions: conditions
        }, this.slot());
      },
      function(err, data) {
        res.render('admin/campaign/list', _(req.query).extend({
          campaigns: data.results,
          pagination: {
            page: page,
            pageCount: Math.ceil(data.totalCount / limit)
          }
        }));
      },
      next
    );
  });

  router.get('/campaigns/:campaign(\\w+)/:action(show|hide)',
  function(req, res, next) {
    var campaign = req.campaign;
    Steppy(
      function() {
        campaign.set('isHidden', req.param('action') === 'show' ? false : true);
        campaign.save(this.slot());
      },
      function() {
        res.redirect('/campaigns');
      },
      next
    );
  });

  router.route('/campaigns/:campaign(\\w+)')
  .get(function(req, res) {
    res.render('admin/campaign/form', {
      campaign: req.campaign
    });
  })
  .post(function(req, res, next) {
    var campaign = req.campaign;
    Steppy(
      function() {
        campaign.set('description', req.param('description'));
        campaign.save(this.slot());
      },
      function() {
        res.redirect('/campaigns');
      },
      next
    );
  });
};
