'use strict';

var Steppy = require('twostep').Steppy,
  mongoose = require('mongoose'),
  fs = require('fs'),
  Path = require('path');

var csvDir = Path.join(__dirname, '../../public/csv'),
  Report = mongoose.model('Report');

module.exports = function(router) {
  router.param('reportId', function(req, res, next, reportId) {
    Report.findById(reportId, function(err, report) {
      if (err) return next(err);

      req.report = report;
      next();
    });
  });

  // results per page
  var limit = 30;
  router.get('/reports', function(req, res, next) {
    var page = Number(req.query.p || 1);

    Steppy(
      function() {
        Report.count(this.slot());
        Report.find(null, null, {
          limit: 30,
          skip: limit * (page - 1)
        }).sort({createDate: -1}).exec(this.slot());
      },
      function(err, count, items) {
        res.render('admin/report/list', {
          reports: items,
          pagination: {
            page: page,
            pageCount: Math.ceil(count / limit)
          }
        });
      },
      next
    );
  });

  router.get('/reports/:reportId([a-f0-9]+)/download',
  function(req, res, next) {
    Steppy(
      function() {
        res.writeHead(200, {
          'Content-Type': 'application/zip',
          'Content-Disposition': 'attachment; filename=' + req.report.fileName
        });
        var stream = fs.createReadStream(
          Path.join(csvDir, req.report.fileName));
        stream.pipe(res);
      },
      next
    );
  });
};
