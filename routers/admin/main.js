'use strict';

module.exports = function(router) {
  // enter point of admin block
  router.get('/', function(req, res) {
    res.redirect('/users');
  });
};
