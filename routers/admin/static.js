'use strict';

var mongoose = require('mongoose'),
 helpers = require('../../utils/helpers');

var Static = mongoose.model('Static');

module.exports = function(router) {
  router.post('/static/:page(' + helpers.editablePages.join('|') +
    '|main' + ')',
  function(req, res, next) {
    Static.update({
      key: req.param('page')
    }, {
      $set: {
        title: req.param('title'),
        description: req.param('description'),
        content: req.param('content')
      }
    }, function(err) {
      if (err) return next(err);

      res.json({success: true});
    });
  });
};
