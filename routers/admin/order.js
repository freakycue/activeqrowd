'use strict';

var Steppy = require('twostep').Steppy,
  _ = require('underscore'),
  mongoose = require('mongoose'),
  searchHelper = require('../../utils/helpers').search;

var Order = mongoose.model('Order');

module.exports = function(router) {
  // results per page
  var limit = 30,
    searchOrders = searchHelper({
      model: Order,
      limit: limit
    });

  router.get('/orders', function(req, res, next) {
    var query = req.query,
      page = Number(req.query.p || 1);

    Steppy(
      function() {
        var conditions = _(query).omit('p');
        if (('status' in conditions) && !conditions.status) {
          delete conditions.status;
        }
        searchOrders(null, {
          conditions: conditions,
          page: page,
          sort: {createDate: -1},
          populate: [{
            path: 'user',
            fields: {
              twitterHandle: 1,
              displayName: 1
            }
          }]
        }, this.slot());
      },
      function(err, data) {
        res.render('admin/order/list', {
          orders: data.results,
          query: query,
          pagination: {
            page: page,
            pageCount: Math.ceil(data.totalCount / limit)
          }
        });
      },
      next
    );
  });
};
