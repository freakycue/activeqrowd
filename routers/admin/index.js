'use strict';

var express = require('express');

var router = module.exports = express.Router();

router.all(/.*/, require('../../middleware/ensureLogin'),
  require('../../middleware/requireAdmin'));

require('./campaign')(router);
require('./main')(router);
require('./order')(router);
require('./recipient')(router);
require('./report')(router);
require('./static')(router);
require('./user')(router);
