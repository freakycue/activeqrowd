'use strict';

var Steppy = require('twostep').Steppy,
  _ = require('underscore'),
  mongoose = require('mongoose'),
  searchHelper = require('../../utils/helpers').search;

var User = mongoose.model('User');

module.exports = function(router) {
  // results per page
  var limit = 30;
  var searchUsers = searchHelper({
    model: User,
    limit: limit
  });

  router.get('/users', function(req, res, next) {
    var page = Number(req.query.p || 1);

    Steppy(
      function() {
        var conditions = {};
        if (req.query.isAdmin) {
          conditions.isAdmin = true;
        }
        if (req.query.isUncomplete) {
          conditions.isComplete = false;
        }

        searchUsers(req.query.q, {
          page: page,
          sort: {createDate: -1},
          conditions: conditions
        }, this.slot());
      },
      function(err, data) {
        res.render('admin/user/list', _(req.query).extend({
          users: data.results,
          pagination: {
            page: page,
            pageCount: Math.ceil(data.totalCount / limit)
          }
        }));
      },
      next
    );
  });

  router.post('/admins/add', function(req, res, next) {
    Steppy(
      function() {
        User.findOne({
          twitterHandle: req.param('handle')
        }).sort({createDate: -1}).exec(this.slot());
      },
      function(err, user) {
        user.set('isAdmin', true);
        user.save(this.slot());
      },
      function() {
        res.redirect('/users');
      },
      next
    );
  });

  router.get('/admins/:handle([A-Za-z0-9_]{1,15})/remove',
  function(req, res, next) {
    Steppy(
      function() {
        User.findOne({twitterHandle: req.param('handle')}, this.slot());
      },
      function(err, user) {
        user.set('isAdmin', false);
        user.save(this.slot());
      },
      function() {
        res.redirect('/users');
      },
      next
    );
  });
};
