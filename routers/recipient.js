'use strict';

var mongoose = require('mongoose'),
  Steppy = require('twostep').Steppy;

var Recipient = mongoose.model('Recipient');

module.exports = function(router) {
  router.post('/recipients/search', function(req, res, next) {
    Steppy(
      function() {
        var query = req.param('query');
        if (query) {
          var regex = {$regex: '^' + query, $options: 'i'};
          Recipient.find({
            $or: [{
              name: regex
            }, {
              twitterHandle: regex
            }]
          }, {
            name: 1,
            twitterHandle: 1
          }, {limit: 15}).sort({name: 1}).exec(this.slot());
        } else {
          this.pass([]);
        }
      },
      function(err, recipients) {
        res.json(recipients);
      },
      next
    );
  });
};
