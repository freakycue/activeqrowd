'use strict';

var Steppy = require('twostep').Steppy,
  _ = require('underscore'),
  bitpay = require('../services/bitpay'),
  twitter = require('../services/twitter'),
  config = require('../config')(),
  mongoose = require('mongoose'),
  errors = require('../utils/errors'),
  crypto = require('crypto');

var Order = mongoose.model('Order'),
  Recipient = mongoose.model('Recipient');

module.exports = function(router) {
  router.param('tweet', function(req, res, next, tweetId) {
    twitter.get(tweetId, function(err, tweet) {
      if (err) return next(err);

      req.tweet = tweet;
      next();
    });
  });

  router.post('/tweets/add', function(req, res, next) {
    var status = req.param('status');
    Steppy(
      function() {
        if (!status) throw new errors.ValidationError('Status is empty.');

        if (!/@qrdbt/.test(status)) {
          throw new errors.ValidationError('Mention of @qrdbt needed.');
        }

        if (!(status.match(/@(?!qrdbt)[A-Za-z0-9_]{1,15}/g) || []).length) {
          throw new errors.ValidationError('At least one recipient needed.');
        }

        twitter.send({
          status: status
        }, {
          token: req.user.token,
          tokenSecret: req.user.tokenSecret
        }, this.slot());
      },
      function(err, tweet) {
        res.json(tweet);
      },
      next
    );
  });

  router.get('/tweets/:tweet([a-f0-9]+)', function(req, res, next) {
    var tweet = req.tweet;

    Steppy(
      function() {
        // get recipients
        Recipient.find({
          twitterHandle: {$in: tweet.recipients}
        }, this.slot());
      },
      function(err, recipients) {
        var recipientsHash = _(recipients).indexBy('twitterHandle'),
          unrecognizedRecipients = _(tweet.recipients).filter(function(handle) {
            return !recipientsHash[handle];
          });

        res.render('tweet/view', {
          tweet: tweet,
          recipients: recipients,
          unrecognizedRecipients: unrecognizedRecipients
        });
      },
      next
    );
  });

  router.post('/tweets/:tweet([a-f0-9]+)/pay', function(req, res, next) {
    var tweet = req.tweet;
    var hash = createHash(tweet.id);

    Steppy(
      function() {
        var recipients = req.param('recipients');
        if (!recipients || !recipients.length) {
          throw new Error('at least one recepient required to send card');
        }

        Recipient.find({
          _id: {$in: recipients}
        }, {twitterHandle: 1}, this.slot());
      },
      function(err, recipients) {
        // match selected recipients with tweet recipients
        var recipientsHash = _(tweet.recipients).indexBy(function(recepient) {
          return recepient;
        });
        if (!_(recipients).every(function(recipients) {
          return recipientsHash[recipients.twitterHandle];
        })) {
          throw new Error('recipients does not match.');
        }

        this.pass(recipients);

        // create invoice on bitpay
        var invoice = {
          price: config.bitpay.price * recipients.length,
          currency: 'BTC',
          transactionSpeed: 'high',
          redirectURL: config.basePath + 'payment/success',
          notificationURL: config.basePath + 'orders/update/status',
          itemDesc: 'Post card from activeqrowd.com',
          posData: JSON.stringify({
            hash: hash
          })
        };

        bitpay.invoiceCreate(invoice, this.slot());
      },
      function(err, recipients, invoice) {
        this.pass(invoice);

        // prepare order object
        var order = new Order({
          user: req.user._id,
          tweet: tweet,
          recipients: recipients,
          campaigns: tweet.campaigns,
          invoiceId: invoice.id,
          invoiceUrl: invoice.url,
          hash: hash
        });
        order.save(this.slot());
      },
      function(err, invoice) {
        // redirect to invoice page
        res.redirect(invoice.url);
      },
      next
    );
  });

  router.get('/payment/success', function(req, res) {
    res.redirect('/profile/my');
  });

  function createHash(str) {
    return crypto.createHash('md5').update(str + config.secret).digest('hex');
  }
};
