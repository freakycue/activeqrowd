'use strict';

var Router = require('express').Router,
  passport = require('passport'),
  ensureLoggedOut = require('connect-ensure-login').ensureLoggedOut,
  ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;

var router = module.exports = Router();

router.get('/auth/login', ensureLoggedOut('/'),
function(req, res, next) {
  if (req.query.return_url) {
    req.session.returnTo = req.query.return_url;
  }
  next();
},passport.authenticate('twitter'));

router.get('/auth/twitter/callback',
  ensureLoggedOut('/'), passport.authenticate('twitter', {
    failureRedirect: '/'
  }), function(req, res) {
    if (!req.user.isComplete) {
      res.redirect('/profile/complete');
    } else {
      var redirectUrl = req.session.returnTo || '/';
      delete req.session.returnTo;
      res.redirect(redirectUrl);
    }
  });

router.get('/auth/logout', ensureLoggedIn('/'), function(req, res) {
  req.logout();
  res.redirect('/');
});
