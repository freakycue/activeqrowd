'use strict';

var Steppy = require('twostep').Steppy,
  mongoose = require('mongoose'),
  errors = require('../utils/errors'),
  bitpay = require('../services/bitpay');

var Order = mongoose.model('Order');

module.exports = function(router) {
  router.param('order', function(req, res, next, id) {
    Order.findById(id, function(err, order) {
      if (err) return next(err);

      req.order = order;
      next();
    });
  });

  router.get('/orders/:order([a-f0-9]+)/sync', function(req, res, next) {
    var order = req.order;
    Steppy(
      function() {
        if (!order.user.equals(req.user._id)) {
          throw new errors.ForbiddenError();
        }

        if (order.status === 'complete') return res.redirect('/profile/my');

        this.pass(order);

        bitpay.invoiceGet(order.invoiceId, this.slot());
      },
      function(err, order, invoice) {

        order.set('status', invoice.status);
        order.save(this.slot());
      },
      function() {
        res.redirect('/profile/my');
      },
      next
    );
  });

  /**
   * Notify url, calling by bitpay, when invoice status is changes
   */
  router.post('/orders/update/status',
  function(req, res, next) {
    var invoice = req.body;

    Steppy(
      function() {
        invoice.posData = JSON.parse(invoice.posData);
        // check order object
        Order.findOne({
          invoiceId: invoice.id,
          hash: invoice.posData.hash
        }, this.slot());
      },
      function(err, order) {
        // update order status
        order.set('status', invoice.status);

        order.save(this.slot());
      },
      function() {
        res.send(200, 'OK');
      },
      next
    );
  });
};
