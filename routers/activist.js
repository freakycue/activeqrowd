'use strict';

var Steppy = require('twostep').Steppy,
  twitter = require('../services/twitter'),
  _ = require('underscore'),
  mongoose = require('mongoose');

var Notification = mongoose.model('Notification'),
  User = mongoose.model('User');

module.exports = function(router) {
  router.param('handle', function(req, res, next, handle) {
    if (req.user && (req.user.twitterHandle === handle)) {
      req.activist = req.user;
      next();
    } else {
      twitter.findUsers({
        twitterHandle: handle
      }, null, function(err, activists) {
        if (err) return next(err);

        req.activist = activists[0];
        next();
      });
    }
  });

  router.get('/profile/my', function(req, res) {
    res.redirect('/activists/' + req.user.twitterHandle);
  });

  router.route('/profile/complete')
  .get(function(req, res) {
    if (req.user.isComplete) {
      return returnToOrRedirect(req, res, '/profile/my');
    }
    res.render('activist/complete');
  })
  .post(function(req, res, next) {
    var user = req.user;

    if (user.isComplete) {
      return returnToOrRedirect(req, res, '/profile/my');
    }

    Steppy(
      function() {
        validateUsersAddress(user, req.body.address);

        user.set('isComplete', true);

        user.save(this.slot());
      },
      function() {
        // send registration congrats notification
        var notification = new Notification({
          type: 'registrationComplete',
          twitterHandle: user.twitterHandle
        });
        notification.save(this.slot());
      },
      function() {
        returnToOrRedirect(req, res, '/profile/my');
      },
      next
    );
  });

  function returnToOrRedirect(req, res, redirectUrl) {
    redirectUrl = 'returnTo' in req.session ? req.session.returnTo :
      redirectUrl;
    delete req.session.returnTo;
    res.redirect(redirectUrl);
  }

  router.get('/activists/:handle([A-Za-z0-9_]{1,15})',
  function(req, res, next) {
    var isOwner = req.user &&
      (req.user.twitterHandle === req.activist.twitterHandle),
      user = req.activist;

    Steppy(
      function() {
        twitter.findOrders({user: user.id}, null, {sort: {createDate: -1}},
          this.slot());
      },
      function(err, orders) {
        res.render('activist/view', {
          user: user,
          orders: orders,
          isOwner: isOwner
        });
      },
      next
    );
  });

  router.route('/activists/:handle([A-Za-z0-9_]{1,15})/edit')
  .get(function(req, res) {
    res.render('activist/form');
  })
  .post(function(req, res, next) {
    var user = req.user;
    Steppy(
      function() {
        validateUsersAddress(user, req.param('address'));
        user.save(this.slot());
      },
      function() {
        res.redirect('/profile/my');
      },
      next
    );
  });

  // results per page
  var limit = 15;
  router.get('/activists', function(req, res, next) {
    var page = Number(req.query.p || 1);

    Steppy(
      function() {
        var options = {
          limit: limit,
          skip: limit * (page - 1),
          sort: {'counters.tweets.count': -1}
        };

        var conditions = {};
        if (req.query.q) {
          conditions.$or = [{
            name: {$regex: '^' + req.query.q, $options: 'i'}
          }, {
            twitterHandle: {$regex: '^' + req.query.q, $options: 'i'}
          }];
        }

        twitter.findUsers(conditions, null, options, this.slot());
        User.count(conditions, this.slot());
      },
      function(err, users, count) {
        res.render('activist/list', {
          users: users,
          q: req.query.q,
          pagination: {
            page: page,
            pageCount: Math.ceil(count / limit)
          }
        });
      },
      next
    );
  });

  var requiredFields = {
    name: 1,
    address: 1,
    city: 1,
    state: 1,
    zipCode: 1
  };

  function validateUsersAddress(user, address) {
    _(address).each(function(val, key) {
      if (requiredFields[key] && !val) throw new Error('validation error');

      user.set('address.' + key, val);
    });
  }
};
