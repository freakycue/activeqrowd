'use strict';

var Steppy = require('twostep').Steppy,
  mongoose = require('mongoose'),
  fs = require('fs'),
  path = require('path');

var Campaign = mongoose.model('Campaign'),
  User = mongoose.model('User');

module.exports = function(router) {
  router.get('/generate/sitemap/d9fadf9390bf41d77e24997caf61ce06',
  function(req, res, next) {
    var urls = [{
      url: '',
      changeFreq: 'hourly',
      priority: 1
    }, {
      url: 'activity',
      changeFreq: 'hourly',
      priority: 0.8
    }, {
      url: 'campaigns',
      changeFreq: 'hourly',
      priority: 0.8
    }, {
      url: 'activists',
      changeFreq: 'hourly',
      priority: 0.8
    }, {
      url: 'about',
      changeFreq: 'daily'
    }, {
      url: 'faq',
      changeFreq: 'daily'
    }, {
      url: 'gettingstarted',
      changeFreq: 'daily'
    }, {
      url: 'tos',
      changeFreq: 'daily'
    }, {
      url: 'support',
      changeFreq: 'daily'
    }, {
      url: 'contact',
      changeFreq: 'daily'
    }];

    Steppy(
      function() {
        Campaign.find({}, {name: 1}, this.slot());
        User.find({}, {twitterHandle: 1}, this.slot());
      },
      function(err, campaigns, users) {
        campaigns.forEach(function(campaign) {
          urls.push({
            url: 'campaigns/' + campaign.name,
            changeFreq: 'hourly',
            priority: campaigns.isHidden ? 0.3 : 0.8
          });
        });

        users.forEach(function(user) {
          urls.push({
            url: 'activists/' + user.twitterHandle,
            changeFreq: 'hourly',
            priority: 0.8
          });
        });

        res.render('static/sitemap', {
          urls: urls
        }, this.slot());
      },
      function(err, content) {
        fs.writeFile(path.join(__dirname, '../../public/sitemap.xml'), content,
          'utf8', this.slot());
      },
      function() {
        res.send(200, 'OK');
      },
      next
    );
  });
};
