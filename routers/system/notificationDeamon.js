'use strict';

var Steppy = require('twostep').Steppy,
  twitter = require('../../services/twitter'),
  mongoose = require('mongoose');

var Notification = mongoose.model('Notification');

module.exports = function(router) {
  var lock = false;
  router.get('/send/notifications/d75ca6affd3415b9cfe7534941b10c62',
  function(req, res, next) {
    if (lock) {
      return res.send(200, 'Busy');
    }
    lock = true;

    Steppy(
      function() {
        // find unprocessed notifiations
        Notification.find({isProcessed: false}, {text: 1})
        .sort({createDate: 1}).exec(this.slot());
      },
      function(err, notifications) {
        if (notifications.length) {
          var step = this;
          // send every notification
          notifications.forEach(function(notification) {
            var cb = step.slot();
            var tweet = {
              status: notification.text
            };
            if (notification.inReplyTo) {
              tweet.in_reply_to_status_id = notification.inReplyTo;
            }
            twitter.send(tweet, function(err) {
              if (!err) {
                notification.set('isProcessed', true);
                notification.save(cb);
              } else {
                handleTwitterError(notification, err, cb);
              }
            });
          });
        } else {
          lock = false;
          res.send(200, 'OK');
        }
      },
      function() {
        lock = false;
        res.send(200, 'OK');
      },
      function(err) {
        lock = false;
        next(err);
      }
    );

    function handleTwitterError(notification, err, callback) {
      notification.set('error.statusCode', err.statusCode);
      notification.set('error.message', err.message);

      // duplicate status
      if (err.code === 187) {
        notification.set('isProcessed', true);
      }

      notification.save(callback);
    }
  });
};
