'use strict';

var Steppy = require('twostep').Steppy,
  mongoose = require('mongoose'),
  bitpay = require('../../services/bitpay');

var Order = mongoose.model('Order');

module.exports = function(router) {
  /**
   * Method for synchronization orders statuses with bitpay invoices
   * in case of server crashes
   */
  router.get('/orders/sync/statuses/6816d18a511d8bb921a63b3562b867c2',
  function(req, res, next) {
    Steppy(
      function() {
        // get orders with uncomplete statuses
        Order.find({
          status: {$nin: ['complete', 'expired', 'invalid']}
        }, {
          invoiceId: 1,
          status: 1
        }, this.slot());
      },
      function(err, orders) {
        if (orders.length) {
          this.pass(orders);
          var group = this.makeGroup();
          orders.forEach(function(order) {
            // get invoice for each order
            bitpay.invoiceGet(order.invoiceId, group.slot());
          });
        } else {
          res.send(200, 'OK');
        }
      },
      function(err, orders, invoices) {
        var step = this;

        // update orders statuses
        orders.forEach(function(order, index) {
          var invoice = invoices[index];
          if (order.status !== invoice.status) {
            order.set('status', invoice.status);
            order.save(step.slot());
          }
        });
        this.pass(null);
      },
      function() {
        res.send(200, 'OK');
      },
      next
    );
  });
};
