'use strict';

var Steppy = require('twostep').Steppy,
  mongoose = require('mongoose');

var testUserHandle = 'aq_test_bot',
  testCampaign = 'aqtest',
  testRecipient = 'aq_test_bot',
  User = mongoose.model('User'),
  Order = mongoose.model('Order'),
  Campaign = mongoose.model('Campaign'),
  Recipient = mongoose.model('Recipient'),
  Tweet = mongoose.model('Tweet');

module.exports = function(router) {
  /**
   * clean testing data from database
   */
  router.get('/clean/test/data/1ba9edfff28fda0cd03420fce572df2a',
  function(req, res, next) {
    Steppy(
      function() {
        User.findOne({
          twitterHandle: testUserHandle
        }, null, {noError: true}, this.slot());
      },
      function(err, user) {
        if (user) {
          Order.remove({user: user.id}, this.slot());
          user.remove(this.slot());
        }
        Campaign.remove({name: testCampaign}, this.slot());
        Tweet.remove({'user.twitterHandle': testUserHandle}, this.slot());
        Recipient.remove({twitterHandle: testRecipient}, this.slot());
        Recipient.remove({twitterHandle: 'PavelFrVlasov'}, this.slot());
      },
      function() {
        res.send(200, 'OK');
      },
      next
    );
  });

  /**
   * generate test data
   */
  router.get('/generate/test/data/d113fd004cec5963080d9b8d9e57eba8',
  function(req, res, next) {
    Steppy(
      function() {
        Campaign.findOne({
          name: testCampaign
        }, null, {noError: true}, this.slot());
        Recipient.findOne({
          twitterHandle: testRecipient
        }, null, {noError: true}, this.slot());
      },
      function(err, campaign, recipient) {
        if (!campaign) {
          campaign = new Campaign({
            name: testCampaign,
            isHidden: true
          });
          campaign.save(this.slot());
        }

        if (!recipient) {
          recipient = new Recipient({
            twitterHandle: 'PavelFrVlasov',
            name: 'Pavel Vlasov',
            address: {
              address: 'Budennogo 6, 355',
              additionalAddress: '',
              city: 'Belgorod',
              state: 'Belgorodskaya',
              zipCode: '12345'
            }
          });
          recipient.save(this.slot());
        }
        this.pass(null);
      },
      function() {
        res.send(200, 'OK');
      },
      next
    );
  });

  /**
   * creates test user if not exists
   */
  router.get('/create/test/user/eac1322de8c2cf46f7593d4a843bb127',
  function(req, res, next) {
    Steppy(
      function() {
        User.findOne({
          twitterHandle: testUserHandle
        }, null, {noError: true}, this.slot());
      },
      function(err, user) {
        if (!user) {
          user = new User({
            displayName: 'Aleksey Timchenok',
            isAdmin: false,
            isComplete: true,
            mainImage: 'https://abs.twimg.com/sticky/default_profile_images' +
              '/default_profile_0_normal.png',
            token: '2527121688-6bhlPfOaAInT6eEFm1cFol7aM2GSQgUEG9VkLWq',
            tokenSecret: 'guzMe37cT9QSxu3njhXZAPrR2zdK5HQQSHrSisteTpeZ3',
            twitterHandle: 'aq_test_bot',
            twitterId: '2527121688',
            address: {
              additionalAddress: '',
              address: 'Budennogo 6, 355',
              city: 'Belgorod',
              name: 'Aleksey Timchenok',
              state: 'Belgorodskaya',
              zipCode: '12345'
            }
          });
          user.save(this.slot());
        } else this.pass(null);
      },
      function() {
        res.send(200, 'OK');
      },
      next
    );
  });

  /**
   * update order object
   */
  router.get('/update/order/:invoiceId/' +
    '44227fb1b209e10274474e627630b7b5', function(req, res, next) {
      Steppy(
        function() {
          Order.update({
            invoiceId: req.param('invoiceId')
          }, {
            $set: {
              status: req.param('status'),
              isProcessed: req.param('isProcessed')
            }
          }, this.slot());
        },
        function() {
          res.send(200, 'OK');
        },
        next
      );
  });

  /**
   * add/remove user's admin rights
   */
  router.get('/user/:handle/admin/:action(add|remove)/' +
    'cb4f9bad721b1c588b15ff5e12d86c2f', function(req, res, next) {
      Steppy(
        function() {
          User.findOne({
            twitterHandle: req.param('handle')
          }, this.slot());
        },
        function(err, user) {
          user.set({
            isAdmin: req.param('action') === 'add' ? true : false
          });
          user.save(this.slot());
        },
        function() {
          res.send(200, 'OK');
        },
        next
      );
  });

  /**
   * remove test campaign
   */
  router.get('/remove/test/campaign/fda21c577b28af774d8093dd7f4c12b9',
  function(req, res, next) {
    Steppy(
      function() {
        Campaign.remove({name: testCampaign}, this.slot());  
      },
      function() {
        res.send(200, 'OK');
      },
      next
    );
  });
};
