'use strict';

var Steppy = require('twostep').Steppy,
  _ = require('underscore'),
  config = require('../../config')(),
  QRCode = require('qrcode'),
  fs = require('fs'),
  Path = require('path'),
  moment = require('moment'),
  mongoose = require('mongoose'),
  archiver = require('archiver'),
  mandrill = require('node-mandrill')(config.mandrill.apiKey);

var csvDir = Path.join(__dirname, '../../public/csv'),
  User = mongoose.model('User'),
  Order = mongoose.model('Order'),
  Report = mongoose.model('Report'),
  // Notification = mongoose.model('Notification'),
  Campaign = mongoose.model('Campaign');

module.exports = function(router) {
  router.get('/generate/cards/csv/554789b11bc6084e92c840ac035decf0',
  function(req, res, next) {
    var currentDate = moment().format('YYYY-MMM-DD_HHmm'),
      archiveName = currentDate + '.zip',
      archivePath = Path.join(csvDir, archiveName),
      archive = archiver('zip'),
      output = fs.createWriteStream(archivePath);

    archive.pipe(output);
    archive.on('error', next);

    Steppy(
      function() {
        Order.find({
          status: 'complete',
          isProcessed: false,
          isDeleted: false
        }).populate('user').populate('recipients').exec(this.slot());
      },
      function(err, orders) {
        if (orders.length) {
          this.pass(orders);
          var group = this.makeGroup();
          orders.forEach(function(order) {
            generateQRCodes(archive, order, group.slot());
          });
        } else {
          output.end();
          res.send(200, 'OK');
        }
      },
      function(err, orders, qrs) {
        this.pass(orders);

        var cardsCount = 0;
        // generate csv
        var file = '"' + [
          'OrderIDCode',
          'Campaign',
          'ActivistID',
          'SenderName',
          'SenderAddress1',
          'SenderAddress2',
          'SenderCity',
          'SenderState',
          'SenderZIP',

          'TwitterHandle',
          'Tweet',
          'TweetDateTimeStamp',
          'TweetQRCode',

          'RecipientName',
          'RecipientAddress1',
          'RecipientAddress2',
          'RecipientCity',
          'RecipientState',
          'RecipientZIP',
          'PostalBarCode',
          'CampaignQRCode'
        ].join('","') + '"' + '\n' +
        orders.map(function(order, index) {
          var user = order.user,
            tweet = order.tweet,
            qrCodes = qrs[index];

          return order.recipients.map(function(recepient) {
            var campaigns = (order.campaigns.length ? order.campaigns : [''])
            .join(' ');
            cardsCount++;
            return '"' + [
              order.id,
              campaigns,
              user.id,
              user.address.name,
              user.address.address,
              user.address.additionalAddress || '',
              user.address.city,
              user.address.state,
              user.address.zipCode,

              tweet.user.twitterHandle,
              unescape(tweet.text),
              moment(tweet.createDate).format('hh:mm A - DD MMM YYYY'),
              qrCodes.tweet,

              recepient.name,
              recepient.address.address,
              recepient.address.additionalAddress || '',
              recepient.address.city,
              recepient.address.state,
              recepient.address.zipCode,
              // TODO postal barcode
              '',
              campaigns && qrCodes.campaign
            ].join('","') + '"';
          }).join('\n');
        }).join('\n');

        this.pass(cardsCount);
        output.on('close', this.slot());
        archive.append(file, {name: '-Cards.csv'}).finalize();
      },
      function(err, orders, cardsCount) {
        var ordersIds = _(orders).pluck('_id');
        this.pass(orders);

        // create csv report
        var report = new Report({
          cardsCount: cardsCount,
          fileName: archiveName,
          orders: ordersIds
        });
        report.save(this.slot());

        fs.readFile(archivePath, 'base64', this.slot());

        // mark orders as processed
        Order.update(
          {_id: {$in: ordersIds}},
          {$set: {isProcessed: true}},
          {multi: true},
          this.slot());
      },
      function(err, orders, report, file) {
        if (orders.length) {
          // email report, only if cards was generated
          mandrill('/messages/send', {
            message: {
              to: [{email: config.mandrill.email, name: config.mandrill.name}],
              from_email: 'noreply@activeqrowd.com',
              subject: 'Daily cards report',
              text: moment().format('DD MMM, YYYY') + ': ' +
                report.orders.length + ' orders with ' + report.cardsCount +
                ' cards.',
              attachments: [{
                type: 'application/zip',
                name: report.fileName,
                content: file
              }]
            }
          }, this.slot());

          var step = this;
          _(orders).chain().groupBy(function(order) {
            // inc campaign cards count
            Campaign.update({
              name: {$in: order.campaigns}
            }, {
              $inc: {'counters.cards.count': order.recipients.length}
            }, {multi: true}, step.slot());

            // inc user's cards count
            User.update({
              _id: order.user.id
            }, {
              $inc: {'counters.cards.count': order.recipients.length}
            }, step.slot());

            return order.user.id;
          });
          // .each(function(group) {
          //   prepare notification for user
          //   var user = group[0].user,
          //     notification = new Notification({
          //       type: 'cardsAddedToBatch',
          //       twitterHandle: user.twitterHandle,
          //       user: user._id
          //     });
          //   notification.save(step.slot());
          // });
        } else {
          this.pass(null);
        }
      },
      function() {
        res.send(200, 'OK');
      },
      next
    );

    function generateQRCodes(archive, order, callback) {
      var campaigns = order.campaigns,
        tweet = order.tweet;

      Steppy(
        function() {
          QRCode.draw(tweet.url, this.slot());

          if (campaigns.length) {
            QRCode.draw(config.basePath + 'campaigns/' +
              campaigns[0], this.slot());
          }
        },
        function(err, canvas, campaignCanvas) {
          var result = {
            tweet: order.id + '-tweet-' + order.user.twitterHandle +
              moment(tweet.createDate).format('HHmm_DD_MMM_YY') + '.jpg',
            campaign: order.id + '-campaign-' + campaigns[0] + '.jpg'
          };

          archive.append(canvas.jpegStream(), {
            name: result.tweet
          });

          if (campaignCanvas) {
            archive.append(campaignCanvas.jpegStream(), {
              name: result.campaign
            });
          }

          this.pass(result);
        },
        callback
      );
    }

    var specialCharachtersHash = {
        '&amp;': '&',
        '&gt;': '>',
        '&lt;': '<',
        '&quot;': '"',
        '&#39;': '\''
      },
      entityToCharRegExp = new RegExp('(' +
        Object.keys(specialCharachtersHash).join('|') + ')');

    function unescape(text) {
      return text.replace(entityToCharRegExp, function(match) {
        return specialCharachtersHash[match];
      });
    }
  });
};
