'use strict';

var express = require('express'),
  config = require('../../config')();

var router = module.exports = express.Router();

require('./campaign')(router);
require('./card')(router);
require('./congressman')(router);
require('./notificationDeamon')(router);
require('./order')(router);
require('./sitemap')(router);

/**
 * DANGER ZONE: this routes gives a lot of power
 */
if (config.useTestingRoutes) {
  require('./test')(router);
}
