'use strict';

var mongoose = require('mongoose'),
  Steppy = require('twostep').Steppy;

var Campaign = mongoose.model('Campaign'),
  Tweet = mongoose.model('Tweet');

module.exports = function(router) {
  // create list of related campaign for each campaign
  router.get('/compute/related/campaigns/1949fed5dce0dcb2e4c2fcde652620f9',
  function(req, res, next) {
    Steppy(
      function() {
        Campaign.find({}, {name: 1}, this.slot());
      },
      function(err, campaigns) {
        var funcs = campaigns.map(function(campaign) {
          return function() {
            Steppy(
              function() {
                Tweet.aggregate([{
                  $match: {campaigns: campaign.name}
                }, {
                  $project: {campaigns: 1}
                }, {
                  $unwind: '$campaigns'
                }, {
                  $match: {campaigns: {$ne: campaign.name}}
                }, {
                  $group: {
                    _id: '$campaigns',
                    count: {$sum: 1}
                  }
                }, {
                  $sort: {count: -1}
                }], this.slot());
              },
              function(err, campaigns) {
                campaign.set('relatedCampaigns', campaigns
                .map(function(campaign) {
                  return {
                    name: campaign._id,
                    count: campaign.count
                  };
                }));
                campaign.save(this.slot());
              },
              this.slot()
            );
          };
        });

        if (funcs.length) {
          funcs.push(this.slot());
          Steppy.apply(null, funcs);
        } else this.pass(null);
      },
      function() {
        res.send(200, 'OK');
      },
      next
    );
  });

  // create list of mentions for each campaign
  router.get('/compute/campaigns/mentions/' +
    'f8e4be748bc03d06102325d83c63008c',
  function(req, res, next) {
    Steppy(
      function() {
        Campaign.find({}, {name: 1}, this.slot());
      },
      function(err, campaigns) {
        var funcs = campaigns.map(function(campaign) {
          return function() {
            Steppy(
              function() {
                Tweet.aggregate([{
                  $match: {campaigns: campaign.name}
                }, {
                  $project: {mentions: '$recipients'}
                }, {
                  $unwind: '$mentions'
                }, {
                  $group: {
                    _id: '$mentions',
                    count: {$sum: 1}
                  }
                }, {
                  $sort: {count: -1}
                }], this.slot());
              },
              function(err, mentions) {
                campaign.set('mentions', mentions.map(function(mention) {
                  return {
                    twitterHandle: mention._id,
                    count: mention.count
                  };
                }));
                campaign.save(this.slot());
              },
              this.slot()
            );
          };
        });

        if (funcs.length) {
          funcs.push(this.slot());
          Steppy.apply(null, funcs);
        } else this.pass(null);
      },
      function() {
        res.send(200, 'OK');
      },
      next
    );
  });
};
