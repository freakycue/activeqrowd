'use strict';

var config = require('../../config')(),
  Steppy = require('twostep').Steppy,
  _ = require('underscore'),
  mongoose = require('mongoose'),
  Sunlight = require('../../services/sunlight');

var Recipient = mongoose.model('Recipient'),
  sunlight = new Sunlight(config.sunlight),
  zipHash = {
    'Rayburn House Office Building': '20515',
    'Russell Courtyard': '20510',
    'Russell Senate Office Building': '20510',
    'Cannon House Office Building': '20515',
    'Hart Senate Office Building': '20510',
    'Longworth House Office Building': '20515',
    'Dirksen Senate Office Building': '20510'
  };

module.exports = function(router) {
  router.get('/congressman/sync/89898e9faab7f3791582ca0893291dfc',
  function(req, res, next) {
    var handles = [];
    Steppy(
      function() {
        sunlight.get('legislators', {
          fields: 'first_name',
          twitter_id__exists: true,
          per_page: 50
        }, this.slot());
      },
      function(err, data) {
        var funcs = _(data.page.count).times(function(page) {
          return function() {
            Steppy(
              function() {
                // get congressman batch
                sunlight.get('legislators', {
                  fields: [
                    'first_name',
                    'last_name',
                    'middle_name',
                    'twitter_id',
                    'office'
                  ].join(','),
                  twitter_id__exists: true,
                  per_page: 50,
                  page: page + 1
                }, this.slot());
              },
              function(err, data) {
                // pick their reflection in db
                var congressman = data.results,
                  twitterHandles = _(congressman).pluck('twitter_id');

                handles = handles.concat(twitterHandles);
                this.pass(congressman);

                Recipient.find({
                  twitterHandle: {$in: twitterHandles}
                }, this.slot());
              },
              function(err, congressman, recipients) {
                var recipientsHash = _(recipients).indexBy('twitterHandle'),
                  step = this;

                congressman.forEach(function(congressman) {
                  // fix for broken congressman address
                  // TODO remove it later
                  if (congressman.office === '717 Hart') {
                    congressman.office = '717 Hart Senate Office Building';
                  }

                  var recipient = recipientsHash[congressman.twitter_id];

                  if (recipient) {
                    var name = [
                      congressman.first_name,
                      congressman.middle_name,
                      congressman.last_name
                    ].join(' ');

                    if ((recipient.name !== name) ||
                      (recipient.address.address !== congressman.office)) {
                      recipient.set('name', name);
                      recipient.set('address.address', congressman.office);
                      recipient.set('address.zipCode', getZip(congressman));
                      recipient.save(step.slot());
                    }
                  } else {
                    // if congressman does not exists - create it
                    recipient = new Recipient({
                      twitterHandle: congressman.twitter_id,
                      name: [
                        congressman.first_name,
                        congressman.middle_name,
                        congressman.last_name
                      ].join(' '),
                      address: {
                        address: congressman.office,
                        city: 'Washington',
                        state: 'DC',
                        zipCode: getZip(congressman)
                      }
                    });
                    recipient.save(step.slot());
                  }
                });

                this.pass(null);

                function getZip(congressman) {
                  var parts = (/\d+\w? ([a-zA-Z ]+)$/).exec(congressman.office);

                  if (parts) {
                    if (!zipHash[parts[1]]) {
                      throw new Error('unknown congressman address: ' +
                        congressman.office);
                    } else {
                      return zipHash[parts[1]];
                    }
                  } else {
                    throw new Error('unknown congressman address: ' +
                      congressman.office);
                  }
                }
              },
              this.slot()
            );
          };
        });

        funcs.push(this.slot());
        Steppy.apply(null, funcs);
      },
      function() {
        res.send(200, 'OK');
      },
      next
    );
  });
};
