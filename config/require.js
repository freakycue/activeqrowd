'use strict';

module.exports = function() {
  return {
    paths: {
      'socket.io': 'empty:',
      jquery: 'lib/jquery',
      bootstrap: 'lib/bootstrap.min',
      jqueryTextComplete: 'lib/jquery.textcomplete',
      ckeditor: 'lib/ckeditor/ckeditor'
    },
    shim: {
      bootstrap: ['jquery'],
      jqueryTextComplete: ['jquery'],
      ckeditor: {exports: 'CKEDITOR'}
    }
  };
};
