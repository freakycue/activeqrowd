#!/bin/bash
# Dump Mongo Dbs database every night using mongodump

BAK="/root/backup/"
DIR=`date +"%Y_%m_%d_%H-%M"`

echo $BAK$DIR
mkdir -p $BAK$DIR

export LC_ALL=C
mongodump -d activeqrowd -o $BAK$DIR
cd $BAK
tar -czf $DIR.tar.gz $DIR
rm -r $BAK$DIR
