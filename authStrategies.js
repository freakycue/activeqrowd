'use strict';

var Steppy = require('twostep').Steppy,
  passport = require('passport'),
  TwitterStrategy = require('passport-twitter').Strategy,
  config = require('./config')(),
  mongoose = require('mongoose');

var User = mongoose.model('User'),
  Tweet = mongoose.model('Tweet');

passport.serializeUser(function(user, done) {
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, null, {noError: true}, done);
});

// twitter uathenticate strategy
passport.use(new TwitterStrategy({
  consumerKey: config.twitter.consumerKey,
  consumerSecret: config.twitter.consumerSecret,
  callbackURL: config.basePath + 'auth/twitter/callback',
  userAuthorizationURL: 'https://api.twitter.com/oauth/authorize',
  name: 'twitter'
},
function(token, tokenSecret, profile, done) {
  Steppy(
    function() {
      // check if user already exists
      User.findOne({
        twitterId: profile.id
      }, null, {noError: true}, this.slot());
    },
    function(err, user) {
      // if user does not exists yet - create it
      if (!user) {
        user = new User({
          twitterId: profile.id
        });
        
        // count user's tweets
        Tweet.count({'user.twitterId': user.twitterId}, this.slot());
        // get user's campaigns
        Tweet.aggregate([{
          $match: {'user.twitterId': user.twitterId}
        }, {
          $project: {campaigns: 1}
        }, {
          $unwind: '$campaigns'
        }, {
          $group: {_id: 0, campaigns: {$addToSet: '$campaigns'}}
        }], this.slot());
      }

      this.pass(user);

      // set or update user's fields
      user.set('twitterHandle', profile.username);
      user.set('displayName', profile.displayName);
      user.set('mainImage', profile.photos[0].value);
      user.set('token', token);
      user.set('tokenSecret', tokenSecret);
    },
    function(err, count, campaigns, user) {
      if (user) {
        campaigns = campaigns[0] && campaigns[0].campaigns;
        // initalize user's counter
        user.set('counters.tweets.count', count);
        if (campaigns) {
          user.set('counters.campaigns.count', campaigns.length);
          user.set('counters.campaigns.items', campaigns);
        }
      } else {
        user = count;
      }
      this.pass(user);
      user.save(this.slot());
    },
    done
  );
}));
