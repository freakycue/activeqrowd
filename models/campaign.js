'use strict';

var mongoose = require('mongoose'),
  searchPlugin = require('mongoose-search-plugin');

var CampaignSchema = mongoose.Schema({
  name: {type: String, lowercase: true, required: true},
  description: String,
  counters: {
    tweets: {count: {type: Number, 'default': 0}},
    users: {
      count: {type: Number, 'default': 0},
      // twitter handle
      items: [String]
    },
    cards: {count: {type: Number, 'default': 0}}
  },
  mentions: [{
    _id: false,
    twitterHandle: {type: String, required: true},
    count: {type: Number, 'default': 1}
  }],
  relatedCampaigns: [{
    _id: false,
    name: {type: String, required: true},
    count: {type: Number, 'default': 1}
  }],
  isHidden: {type: Boolean, 'default': false},
  updateDate: {type: Number, 'default': Date.now},
  createDate: {type: Number, 'default': Date.now}
});

CampaignSchema.plugin(searchPlugin, {
  fields: [
    'name',
    'description'
  ]
});

mongoose.model('Campaign', CampaignSchema);

CampaignSchema.pre('save', function(next) {
  this.set('updateDate', Date.now());
  next();
});
