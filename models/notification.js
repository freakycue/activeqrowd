'use strict';

var mongoose = require('mongoose');

var NotificationSchema = mongoose.Schema({
  type: {type: String, enum: [
    'registrationRequired',
    'registrationCompleteRequired',
    'registrationComplete',
    'orderWithNoRecipients',
    'recipientsUnrecognized'
    // 'cardsAddedToBatch'
  ], required: true},
  twitterHandle: {type: String, required: true},
  user: {type: mongoose.Schema.ObjectId, ref: 'User'},
  text: String,
  inReplyTo: String,
  include: String,
  isProcessed: {type: Boolean, 'default': false},
  error: {},
  createDate: {type: Number, 'default': Date.now}
});

mongoose.model('Notification', NotificationSchema);

var strategies = {
  registrationRequired: function(doc, callback) {
    doc.set('text', '@' + doc.twitterHandle + ' register at activeqrowd.com' +
      ' to extend your online actions into offline impact. It\'ll only take' +
      ' a second!');
    callback();
  },
  registrationCompleteRequired: function(doc, callback) {
    doc.set('text', '@' + doc.twitterHandle + ', your activeqrowd.com' +
      ' registration is incomplete. Finish it and I can increase your' +
      ' Tweets\' impact. :)');
    callback();
  },
  registrationComplete: function(doc, callback) {
    doc.set('text', '@' + doc.twitterHandle + ' – registration complete;' +
      ' thank you. Take Action: Start Tweeting in support of a Campaign!' +
      ' ActiveQrowd.com');
    callback();
  },
  orderWithNoRecipients: function(doc, callback) {
    doc.set('text', '@' + doc.twitterHandle + ', thanks for the Mention.' +
      ' I\'m confused: Your Tweet has no recipient. Did you mean to Order?');
    callback();
  },
  recipientsUnrecognized: function(doc, callback) {
    doc.set('text', '@' + doc.twitterHandle + ' I don\'t have an address' +
      ' to send your Tweet to. If you do, email add@activeqrowd.com');
    callback();
  }
  // cardsAddedToBatch: function(doc, callback) {
  //   doc.set('text', '@' + doc.twitterHandle +
  //     ' - order verified. Your Tweet' +
  //     ' in support of #' + doc.include + ' is queued to print. Arrival' +
  //     ' within 3 days.');
  //   callback();
  // }
};

NotificationSchema.pre('save', function(next) {
  if (this.isNew) {
    strategies[this.type](this, next);
  } else {
    next();
  }
});
