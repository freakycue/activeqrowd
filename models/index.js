'use strict';

var mongoose = require('mongoose'),
  errors = require('../utils/errors'),
  _ = require('underscore');

['findOne', 'findById'].forEach(function(method) {
  mongoose.Model[method] = _(mongoose.Model[method]).wrap(function(originalFn) {
    var args = [].slice.call(arguments, 1),
      cb = args[args.length - 1],
      query;

    if (_(cb).isFunction()) {
      query = originalFn.apply(this, args.slice(0, -1));
      prepareQuery(4);
      return query.exec(cb);
    } else {
      query = originalFn.apply(this, args);
      prepareQuery(3);
      return query;
    }

    function prepareQuery(optionsIndex) {
      query.method = method;
      if (args.length === optionsIndex) {
        query.noError = args[2] && args[2].noError;
      }
    }
  });
});

mongoose.Query.prototype.exec = _(mongoose.Query.prototype.exec)
.wrap(function(exec, cb) {
  if (this.method && !this.noError) {
    cb = _(cb).wrap(function(cb, err, res) {
      if (!res) {
        return cb(err || new errors.NotFoundError());
      }
      cb(err, res);
    });
  }
  exec.call(this, cb);
});

[
  'order',
  'user',
  'notification',
  'recipient',
  'report',
  'twitterCache',
  'campaign',
  'tweet',
  'static'
].forEach(function(model) {
  require('./' + model);
});
