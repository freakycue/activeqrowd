'use strict';

var mongoose = require('mongoose'),
  searchPlugin = require('mongoose-search-plugin');

var UserSchema = mongoose.Schema({
  twitterId: {type: String, required: true},
  twitterHandle: {type: String, required: true},
  displayName: {type: String, required: true},
  mainImage: {type: String, required: true},
  token: {type: String, required: true},
  tokenSecret: {type: String, required: true},
  isComplete: {type: Boolean, 'default': false},
  isAdmin: {type: Boolean, 'default': false},
  address: {
    name: String,
    address: String,
    additionalAddress: String,
    city: String,
    state: String,
    zipCode: String
  },
  counters: {
    tweets: {count: {type: Number, 'default': 0}},
    cards: {count: {type: Number, 'default': 0}},
    campaigns: {
      count: {type: Number, 'default': 0},
      items: [String]
    }
  },
  createDate: {type: Number, 'default': Date.now},
  updateDate: {type: Number, 'default': Date.now}
});

UserSchema.plugin(searchPlugin, {
  fields: [
    'twitterHandle',
    'displayName',
    'address.name',
    'address.address',
    'address.additionalAddress',
    'address.city',
    'address.state',
    'address.zipCode'
  ]
});

mongoose.model('User', UserSchema);

UserSchema.pre('save', function(next) {
  this.set('updateDate', Date.now());
  next();
});
