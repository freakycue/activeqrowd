'use strict';

var mongoose = require('mongoose');

var ReportSchema = mongoose.Schema({
  cardsCount: {type: Number, required: true},
  fileName: {type: String, required: true},
  orders: [{type: mongoose.Schema.ObjectId, ref: 'Order'}],
  createDate: {type: Number, 'default': Date.now}
});

mongoose.model('Report', ReportSchema);
