'use strict';

var mongoose = require('mongoose');

var TwitterCacheSchema = mongoose.Schema({
  key: {type: String, required: true},
  response: {},
  expires: Number,
  ttl: {type: Date, 'default': function() {
    return new Date();
  }}
});

mongoose.model('TwitterCache', TwitterCacheSchema);
