'use strict';

var mongoose = require('mongoose');

var OrderSchema = mongoose.Schema({
  user: {type: mongoose.Schema.ObjectId, ref: 'User', requried: true},
  tweet: {},
  recipients: [{type: mongoose.Schema.ObjectId, ref: 'Recipient'}],
  campaigns: [String],
  invoiceId: {type: String, requried: true},
  invoiceUrl: {type: String, requried: true},
  hash: {type: String, requried: true},
  status: {type: String, 'default': 'new'},
  isProcessed: {type: Boolean, 'default': false},
  isDeleted: {type: Boolean, 'default': false},
  createDate: {type: Number, 'default': Date.now}
});

mongoose.model('Order', OrderSchema);
