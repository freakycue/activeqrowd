'use strict';

var mongoose = require('mongoose');

var StaticSchema = mongoose.Schema({
  key: {type: String, required: true},
  content: {type: String, required: true},
  title: {type: String, required: true},
  description: {type: String, required: true}
});

mongoose.model('Static', StaticSchema);
