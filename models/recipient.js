'use strict';

var mongoose = require('mongoose'),
  searchPlugin = require('mongoose-search-plugin');

var RecipientSchema = mongoose.Schema({
  twitterHandle: {type: String, required: true},
  name: {type: String, required: true},
  address: {
    address: {type: String, required: true},
    additionalAddress: String,
    city: {type: String, required: true},
    state: {type: String, required: true},
    zipCode: {type: String, required: true}
  },
  createDate: {type: Number, 'default': Date.now}
});

RecipientSchema.plugin(searchPlugin, {
  fields: [
    'twitterHandle',
    'name',
    'address.address',
    'address.additionalAddress',
    'address.city',
    'address.state',
    'address.zipCode'
  ]
});

mongoose.model('Recipient', RecipientSchema);
