'use strict';

var mongoose = require('mongoose');

var TweetSchema = mongoose.Schema({
  tweetId: {type: String, required: true},
  user: {
    twitterId: {type: String, required: true},
    twitterHandle: {type: String, required: true},
    displayName: {type: String, required: true},
    avatar: {type: String, required: true}
  },
  text: {type: String, required: true},
  campaigns: [String],
  recipients: [String],
  entities: {type: Object, required: true},
  createDate: {type: Number, 'default': Date.now}
});

mongoose.model('Tweet', TweetSchema);
