'use strict';

var moment = require('moment'),
  _ = require('underscore');

// 20 sec timeout
casper.options.waitTimeout = 20000;

// handle javascript errors
exports.javascriptErrors = [];
casper.on('page.error', function(msg, trace) {

  this.echo('Error: ' + msg, 'ERROR');
  this.echo('file: ' + trace[0].file, 'WARNING');
  this.echo('line: ' + trace[0].line, 'WARNING');
  this.echo('function: ' + trace[0]['function'], 'WARNING');

  exports.javascriptErrors.push({
    url: this.getCurrentUrl(),
    msg: msg,
    file: trace[0].file,
    line: trace[0].line,
    'function': trace[0]['function']
  });
});
// handle 404 resources
casper.on('resource.received', function(request) {
  if (request.status >= 400) {
    this.echo('Failed to load resource ' + request.url + ' status: ' +
      request.status, 'ERROR');
  }
});

// capture screenshot helper
exports.capture = function(name) {
  casper.capture('./test/screenshots/' + moment().format('YYYY-MMM-DD_HH-mm') +
    (name || '') + '.png');
};

// handle errors
exports.onError = function(msg) {
  return function() {
    if (casper.started) {
      exports.capture(this);
      this.echo(msg || 'exit with error', 'ERROR');
    }
    casper.exit(1);
  };
};
// casper.options.onWaitTimeout = exports.onError('exited with timeout error');
// casper.test.on('fail', exports.onError('test failed'));

var config = require('./config')(casper.cli.options.env);
exports.config = config;

exports.createUrl = function(url) {
  return config.basePath + url;
};

/**
 * creates url regexp for wait and assert url methods
 */
exports.createMatchUrl = function(url) {
  return new RegExp('^' + config.basePath + url);
};

exports.createTestUser = function() {
  exports.log('Create test user, if not exists');
  casper.thenOpen(exports.createUrl(
    'system/create/test/user/eac1322de8c2cf46f7593d4a843bb127'));
};

exports.changeOrderStatus = function(id, params) {
  var query = _(params).map(function(val, key) {
    return key + '=' + val;
  }).join('&');
  casper.thenOpen(exports.createUrl('system/update/order/' + id + '/' +
    '44227fb1b209e10274474e627630b7b5?' + query));
};

exports.generateCards = function() {
  casper.thenOpen(exports.createUrl(
    'system/generate/cards/csv/554789b11bc6084e92c840ac035decf0'));
};

exports.generateMentions = function() {
  casper.thenOpen(exports.createUrl('system/compute/campaigns/mentions/' +
    'f8e4be748bc03d06102325d83c63008c'));
};

exports.userAdminRights = function(handle, action) {
  exports.log(action + ' ' + handle + '\'s admin rights');
  casper.thenOpen(exports.createUrl('system/user/' + handle + '/admin/' +
    action + '/' + 'cb4f9bad721b1c588b15ff5e12d86c2f'));
};

exports.removeTestCampaign = function() {
  exports.log('Remove test campaign');
  casper.thenOpen(exports.createUrl('system/remove/test/campaign/' +
    'fda21c577b28af774d8093dd7f4c12b9'));
};

exports.getFieldContent = function(selector) {
  return casper.evaluate(function(selector) {
    return document.querySelectorAll(selector)[0].value;
  }, selector);
};

exports.assertObjects = function(obj1, obj2, method) {
  method = method || 'assertEqual';
  _(obj1).each(function(val, key) {
    if (_(val).isObject()) {
      return exports.assertObjects(val, obj2[key], method);
    }
    casper.test[method](val, obj2[key]);
  });
};

var keyRegExp = /^(.+)\[(.+)\]$/;
exports.convertFormValues = function(obj) {
  _(obj).each(function(val, key) {
    if (keyRegExp.test(key)) {
      var parts = keyRegExp.exec(key);
      if (!obj[parts[1]]) {
        obj[parts[1]] = {};
      }
      obj[parts[1]][parts[2]] = val;
      delete obj[key];
    }
  });
  return obj;
};

exports.convertToFormValues = function(obj) {
  function _convertToFormValues(path, from, to) {
    _(from).each(function(val, key) {
      var newPath = path ? path + '[' + key + ']' : key;
      if (_(val).isObject()) {
        return _convertToFormValues(newPath, from[key], to);
      }
      to[newPath] = val;
    });
  }

  var result = {};

  _convertToFormValues('', obj, result);
  return result;
};

exports.log = function(msg, level) {
  casper.then(function() {
    this.echo(msg, level || 'INFO_BAR');
  });
};
