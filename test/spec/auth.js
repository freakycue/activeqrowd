'use strict';

var helpers = require('../helpers'),
  _ = require('../proxies/underscore'),
  user = helpers.config.user,
  MainPage = require('../pages/main'),
  ProfileCompletePage = require('../pages/profile/complete'),
  // TweetViewPage = require('../pages/tweet/view'),
  ProfileViewPage = require('../pages/profile/view'),
  ProfileEditPage = require('../pages/profile/edit');

/**
 * pages
 */
var mainPage = new MainPage(),
  profileCompletePage = new ProfileCompletePage(),
  // tweetViewPage = new TweetViewPage(),
  profileViewPage = new ProfileViewPage(user.handle),
  profileEditPage = new ProfileEditPage(user.handle);

casper.test.begin('auth test', function(test) {
  /**
   * test registration flow and profile complete
   */
  casper.start();
  helpers.log('Test registration flow');
  mainPage.open();
  mainPage.login(user);
  casper.then(function() {
    test.comment('Redirect to profile complete page.');
  });
  profileCompletePage.init();
  // casper.then(function() {
  //   test.comment('Test redirects for uncomplete user');
  // });
  // mainPage.open();
  // mainPage.clickPay();
  // casper.then(function() {
  //   test.comment('Redirect to complete profile page.');
  // });
  // profileCompletePage.init();
  profileCompletePage.fillAddressForm(user.address, true);
  // tweetViewPage.init();

  /**
   * test admin permissions
   */
  // helpers.log('Test admin permissions');
  // casper.thenOpen(helpers.createUrl('admin'), function(response) {
  //   test.comment('check admin permissions');
  //   helpers.capture('permission');
  //   test.assertEqual(response.status, 403);
  // });

  /**
   * edit profile
   */
  helpers.log('Test profile edition');
  mainPage.open();
  mainPage.navigateTo('profile');
  profileViewPage.init();
  profileViewPage.navigateTo('edit');

  profileEditPage.init();
  var newAddress = _({}).extend(user.address, {
    address: 'Boulevard of Broken Dreams',
    city: 'Los Angeles',
    state: 'CA'
  });
  casper.then(function() {
    var formAddress = profileEditPage.getAddress();
    _(user.address).each(function(val, key) {
      test.assertEqual(val, formAddress[key]);
    });
  });
  profileEditPage.fillAddressForm(newAddress, true);

  profileViewPage.init();
  casper.then(function() {
    var profileAddress = profileViewPage.getAddress();
    _(newAddress).each(function(val, key) {
      test.assertEqual(val, profileAddress[key]);
    });
  });

  /**
   * login/logout test
   */
  helpers.log('Test login/logout flow');
  mainPage.open();
  mainPage.logout();
  mainPage.login(user);
  mainPage.logout();

  casper.run(function() {
    test.done();
  });
});
