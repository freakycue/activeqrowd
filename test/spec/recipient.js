'use strict';

var helpers = require('../helpers'),
  _ = require('../proxies/underscore'),
  user = helpers.config.user,
  recipient = helpers.config.recipient,
  MainPage = require('../pages/main'),
  AdminProfileListPage = require('../pages/admin/profile/list'),
  AdminRecipientListPage = require('../pages/admin/recipient/list'),
  AdminRecipientFormPage = require('../pages/admin/recipient/form');

var mainPage = new MainPage(),
  adminProfileListPage = new AdminProfileListPage(),
  adminRecipientListPage = new AdminRecipientListPage(),
  adminRecipientFormPage = new AdminRecipientFormPage();

casper.test.begin('Admin recipient test', function(test) {
  casper.start();

  helpers.createTestUser();
  helpers.userAdminRights(user.handle, 'add');

  mainPage.open();
  mainPage.login(user);

  mainPage.navigateTo('admin');
  adminProfileListPage.init();
  adminProfileListPage.navigateTo('recipients');
  adminRecipientListPage.init();

  helpers.log('Create recipient');
  adminRecipientListPage.fillRecipientForm(recipient);
  adminRecipientListPage.init();

  helpers.log('Search recipient by name');
  adminRecipientListPage.searchRecipients(recipient.name);
  adminRecipientListPage.init();

  helpers.log('Check recipient');
  var newRecipient;
  casper.then(function() {
    newRecipient = adminRecipientListPage
    .getRecipient(recipient.twitterHandle);
    helpers.assertObjects(recipient, newRecipient);
  });

  helpers.log('Navigate for recipient form');
  casper.then(function() {
    adminRecipientListPage.editRecipient(recipient.twitterHandle);
  });
  adminRecipientFormPage.init();
  helpers.log('Check recipient in form page');
  casper.then(function() {
    newRecipient = adminRecipientFormPage.getRecipient(recipient.twitterHandle);
    helpers.assertObjects(recipient, newRecipient);
  });

  helpers.log('Edit recipient');
  var editedRecipient = _({}).extend(recipient, {
    name: 'Sergey Michalok'
  });
  adminRecipientFormPage.fillRecipientForm(editedRecipient);

  helpers.log('Check edited recipient');
  adminRecipientListPage.init();
  adminRecipientListPage.searchRecipients(editedRecipient.name);
  adminRecipientListPage.init();
  casper.then(function() {
    newRecipient = adminRecipientListPage
    .getRecipient(editedRecipient.twitterHandle);
    helpers.assertObjects(editedRecipient, newRecipient);
  });

  helpers.log('Remove recipient');
  casper.then(function() {
    adminRecipientListPage.removeRecipient(editedRecipient.twitterHandle);
  });
  adminRecipientListPage.init();

  helpers.userAdminRights(user.handle, 'remove');
  helpers.log('Logout');
  mainPage.open();
  mainPage.logout();

  casper.run(function() {
    test.done();
  });
});
