'use strict';

var helpers = require('../helpers'),
  MainPage = require('../pages/main'),
  TweetViewPage = require('../pages/tweet/view'),
  ProfileListPage = require('../pages/profile/list'),
  CampaignViewPage = require('../pages/campaign/view');

var campaign = helpers.config.campaign,
  user = helpers.config.user,
  mainPage = new MainPage(),
  tweetViewPage = new TweetViewPage(),
  profileListPage = new ProfileListPage(),
  campaingViewPage = new CampaignViewPage(campaign);

casper.test.begin('Campaign test', function(test) {
  casper.start();
  helpers.createTestUser();

  mainPage.open();
  mainPage.login(user);

  helpers.log('Get current campaign counters');
  campaingViewPage.open();
  var counters;
  casper.then(function() {
    counters = campaingViewPage.getCounters();
  });

  helpers.log('Get current user counters');
  mainPage.open();
  mainPage.navigateTo('activists');
  profileListPage.init();
  var userCounters;
  profileListPage.searchUsers(user.handle);
  casper.then(function() {
    userCounters = profileListPage.getUserCounters(user.handle);
  });

  helpers.log('Tweet in support of campaign');
  mainPage.open();
  mainPage.type(' @PavelFrVlasov #' + campaign + ' ' + Date.now());
  mainPage.submitTweet();
  tweetViewPage.init();
  helpers.log('Tweet again and pay for tweet');
  mainPage.open();
  mainPage.type(' @PavelFrVlasov #' + campaign + ' +1 ' + Date.now());
  mainPage.submitTweet();
  tweetViewPage.init();
  tweetViewPage.pay({
    status: 'complete',
    isProcessed: false
  });

  helpers.log('Generate cards');
  helpers.generateCards();

  helpers.log('Generate campaign mentions');
  helpers.generateMentions();

  helpers.log('Check new campaign counters');
  campaingViewPage.open();
  casper.then(function() {
    var newCounters = campaingViewPage.getCounters();
    test.assertEqual(counters.tweets + 2, newCounters.tweets);
    test.assertEqual(counters.users + 1, newCounters.users);
    test.assertEqual(counters.cards + 1, newCounters.cards);
  });

  helpers.log('Check campaign mentions');
  casper.then(function() {
    test.assertTruthy(campaingViewPage.getMentions()
      .indexOf('@PavelFrVlasov') > -1);
  });

  helpers.log('Check new profile counters');
  profileListPage.open();
  profileListPage.searchUsers(user.handle);
  casper.then(function() {
    var newUserCounters = profileListPage.getUserCounters(user.handle);
    test.assertEqual(userCounters.tweets + 2, newUserCounters.tweets);
    test.assertEqual(userCounters.campaigns + 1, newUserCounters.campaigns);
    test.assertEqual(userCounters.cards + 1, newUserCounters.cards);
  });

  /**
   * logout
   */
  mainPage.open();
  mainPage.logout();

  casper.run(function() {
    test.done();
  });
});
