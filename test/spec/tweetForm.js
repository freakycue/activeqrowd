'use strict';

var helpers = require('../helpers'),
  user = helpers.config.user,
  MainPage = require('../pages/main'),
  TweetViewPage = require('../pages/tweet/view');

var mainPage = new MainPage(),
  tweetViewPage = new TweetViewPage();

casper.test.begin('Tweet entry field test', function(test) {
  casper.start();
  helpers.createTestUser();
  mainPage.open();
  mainPage.login(user);

  /**
   * test recipients autosuggest
   */
  mainPage.init();
  helpers.log('Test recipients autosuggest');
  mainPage.type('@pavelf');


  var recipientHandle;
  mainPage.selectAutosuggestItem('recipient', 0, function(recipient) {
    recipientHandle = recipient;
  });
  casper.then(function() {
    test.assertTruthy(mainPage.getTweetText().indexOf(recipientHandle) > -1);
  });

  /**
   * test campaign autosuggest
   */
  helpers.log('Test campaign autosuggest');
  mainPage.type('#AQTes');
  var campaign;
  mainPage.selectAutosuggestItem('campaign', 0, function(selectedCampaign) {
    campaign = selectedCampaign;
  });
  casper.then(function() {
    test.assertTruthy(mainPage.getTweetText().indexOf(campaign) > -1);
  });

  /**
   * test ban for tweet without recipients and recommendation to add #campaign
   */
  helpers.log('Test ban for tweet without recipients');
  mainPage.open();
  mainPage.type('testing tweet ' + Date.now());
  mainPage.submitTweet();
  mainPage.waitForPopup();
  casper.then(function() {
    test.assertTruthy(mainPage.getPopupContent()
      .indexOf('Add at least one recipient.') > -1);
  });
  mainPage.type(' @PavelFrVlasov');
  mainPage.submitTweet();
  mainPage.waitForPopup();
  casper.then(function() {
    test.assertTruthy(mainPage.getPopupContent()
      .indexOf('We recommend a #campaign hashtag so your Tweet can be' +
        ' searched for more easily.') > -1);
  });
  mainPage.submitTweet();
  helpers.log('Check redirect to tweet page', 'INFO');
  tweetViewPage.init();

  /**
   * logout
   */
  mainPage.open();
  mainPage.logout();

  casper.run(function() {
    test.done();
  });
});
