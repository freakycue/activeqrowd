'use strict';

var helpers = require('../helpers'),
  user = helpers.config.user,
  MainPage = require('../pages/main'),
  TweetViewPage = require('../pages/tweet/view'),
  AdminProfileListPage = require('../pages/admin/profile/list'),
  AdminCampaignListPage = require('../pages/admin/campaign/list');

var campaign = helpers.config.campaign,
  mainPage = new MainPage(),
  tweetViewPage = new TweetViewPage(),
  adminProfileListPage = new AdminProfileListPage(),
  adminCampaignListPage = new AdminCampaignListPage();

casper.test.begin('Admin campaign test', function(test) {
  casper.start();

  helpers.createTestUser();
  helpers.userAdminRights(user.handle, 'add');
  helpers.removeTestCampaign();

  mainPage.open();
  mainPage.login(user);
  mainPage.init();

  helpers.log('Tweet in support of test campaign');
  mainPage.type('tweet for @' + helpers.config.recipient.twitterHandle +
    ' in support of #' + campaign + ' ' + Date.now());
  mainPage.submitTweet();
  tweetViewPage.init();

  helpers.log('Check campaign in admin page');
  mainPage.open();
  mainPage.navigateTo('admin');
  adminProfileListPage.init();
  adminProfileListPage.navigateTo('campaigns');
  adminCampaignListPage.init();
  adminCampaignListPage.searchCampaigns(campaign);
  var campaignObject;
  casper.then(function() {
    campaignObject = adminCampaignListPage.getCampaign(campaign);
    test.assertEqual(campaignObject.name, campaign.toLowerCase());
    test.assertEqual(campaignObject.description, '');
    test.assertEqual(campaignObject.counters.tweets, 1);
    test.assertEqual(campaignObject.counters.users, 1);
    test.assertEqual(campaignObject.counters.cards, 0);
  });

  casper.run(function() {
    test.done();
  });
});
