'use strict';

var BasePage = require('../base'),
  patchedRequrie = patchRequire(require),
  utils = patchedRequrie('utils');

var BaseAdminPage = module.exports = function() {
  BasePage.apply(this, arguments);

  this.url = 'admin';

  this.links = {
    activists: '.js-activists',
    recipients: '.js-recipients',
    reports: '.js-reports',
    campaigns: '.js-campaigns'
  };
};
utils.inherits(BaseAdminPage, BasePage);
