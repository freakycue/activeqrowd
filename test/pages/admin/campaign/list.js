'use strict';

var BaseAdminPage = require('../base'),
  _ = require('underscore'),
  patchedRequire = patchRequire(require),
  utils = patchedRequire('utils');

var AdminCampaignListPage = module.exports = function() {
  BaseAdminPage.apply(this, arguments);

  this.url = this.url + '/campaigns';

  this.elements = _(this.elements).extend({
    searchForm: 'form',
    isHidden: '[name="isHidden"]',
    campaign: 'tr',
    campaignName: '.js-campaign-name',
    campaignDescription: '.js-campaign-description',
    campaignTweetsCount: '.js-campaign-tweets-count',
    campaignUsersCount: '.js-campaign-users-count',
    campaignCardsCount: '.js-campaign-cards-count'
  });
};
utils.inherits(AdminCampaignListPage, BaseAdminPage);

AdminCampaignListPage.prototype.searchCampaigns = function(keywords, isHidden) {
  var self = this;
  this.casper.then(function() {
    if (isHidden) {
      this.click(self.elements.isHidden);
    }
    this.fill(self.elements.searchForm, {
      q: keywords
    }, true);
  });
};

AdminCampaignListPage.prototype.getCampaign = function(campaignName) {
  var selector = this.elements.campaign + '[data-campaign="' +
    campaignName.toLowerCase() + '"]';
  return {
    name: this.fetchText(selector, 'campaignName'),
    description: this.fetchText(selector, 'campaignDescription'),
    counters: {
      tweets: Number(this.fetchText(selector, 'campaignTweetsCount')),
      users: Number(this.fetchText(selector, 'campaignUsersCount')),
      cards: Number(this.fetchText(selector, 'campaignCardsCount'))
    }
  };
};
