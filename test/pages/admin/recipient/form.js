'use strict';

var BaseAdminPage = require('../base'),
  _ = require('underscore'),
  patchedRequire = patchRequire(require),
  utils = patchedRequire('utils');

var AdminRecipientFormPage = module.exports = function() {
  BaseAdminPage.apply(this, arguments);

  this.url = this.url + '/recipients/([a-f0-9]+)';

  this.elements = _(this.elements).extend({
    recipientForm: '#recipient-form'
  });
};
utils.inherits(AdminRecipientFormPage, BaseAdminPage);

AdminRecipientFormPage.prototype.getRecipient = function() {
  return this.helpers.convertFormValues(this.casper
    .getFormValues(this.elements.recipientForm));
};

AdminRecipientFormPage.prototype.fillRecipientForm = function(recipient) {
  var self = this;
  this.casper.then(function() {
    this.fill(self.elements.recipientForm,
      self.helpers.convertToFormValues(recipient), true);
  });
};
