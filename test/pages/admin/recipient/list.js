'use strict';

var BaseAdminPage = require('../base'),
  patchedRequire = patchRequire(require),
  _ = require('underscore'),
  utils = patchedRequire('utils');

var AdminRecipientListPage = module.exports = function() {
  BaseAdminPage.apply(this, arguments);

  this.url = this.url + '/recipients';

  this.elements = _(this.elements).extend({
    recipientForm: '#recipient-form',
    searchForm: '#search-form',
    recipient: 'tr',
    recipientHandle: '.js-recipient-handle',
    recipientName: '.js-recipient-name',
    recipientAddressAddress: '.js-recipient-address-address',
    recipientAddressAdditionalAddress: '.js-recipient-address-additional' +
      '-address',
    recipientAddressCity: '.js-recipient-address-city',
    recipientAddressState: '.js-recipient-address-state',
    recipientAddressZipCode: '.js-recipient-address-zip-code',
    editRecipient: 'a[title="edit"]',
    removeRecipient: 'a[title="remove"]'
  });
};
utils.inherits(AdminRecipientListPage, BaseAdminPage);

AdminRecipientListPage.prototype.fillRecipientForm = function(recipient) {
  var self = this;
  this.casper.then(function() {
    this.fill(self.elements.recipientForm, _(recipient).chain()
      .pick('twitterHandle', 'name')
      .extend({
        'address[address]': recipient.address.address,
        'address[additionalAddress]': recipient.address.additionalAddress,
        'address[city]': recipient.address.city,
        'address[state]': recipient.address.state,
        'address[zipCode]': recipient.address.zipCode
      }).value(), true);
  });
};

AdminRecipientListPage.prototype.searchRecipients = function(keywords) {
  var self = this;
  this.casper.then(function() {
    this.fill(self.elements.searchForm, {
      q: keywords
    }, true);
  });
};

AdminRecipientListPage.prototype._getRecipientSelector = function(handle) {
  return this.elements.recipient + '[data-recipient="' + handle + '"] ';
};

/**
 * get recipient from list by it's handle
 */
AdminRecipientListPage.prototype.getRecipient = function(handle) {
  var selector = this._getRecipientSelector(handle),
    elements = this.elements;
  return {
    twitterHandle: this.casper.fetchText(selector + elements.recipientHandle),
    name: this.casper.fetchText(selector + elements.recipientName),
    address: {
      address: this.casper.fetchText(selector +
        elements.recipientAddressAddress),
      additionalAddress: this.casper.fetchText(selector +
        elements.recipientAddressAdditionalAddress),
      city: this.casper.fetchText(selector + elements.recipientAddressCity),
      state: this.casper.fetchText(selector + elements.recipientAddressState),
      zipCode: this.casper.fetchText(selector +
        elements.recipientAddressZipCode)
    }
  };
};

AdminRecipientListPage.prototype.editRecipient = function(handle) {
  this.casper.click(this._getRecipientSelector(handle) +
    this.elements.editRecipient);
};

AdminRecipientListPage.prototype.removeRecipient = function(handle) {
  this.casper.click(this._getRecipientSelector(handle) +
    this.elements.removeRecipient);
};
