'use strict';

var BaseAdminPage = require('../base'),
  patchedRequire = patchRequire(require),
  utils = patchedRequire('utils');

var AdminProfileListPage = module.exports = function() {
  BaseAdminPage.apply(this, arguments);

  this.url = this.url + '/users';
};
utils.inherits(AdminProfileListPage, BaseAdminPage);
