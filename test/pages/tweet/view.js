'use strict';

var BasePage = require('../base'),
  _ = require('underscore'),
  patchedRequire = patchRequire(require),
  utils = patchedRequire('utils');

var TweetViewPage = module.exports = function() {
  BasePage.apply(this, arguments);
  this.url = 'tweets/(\\d+)';

  this.elements = _(this.elements).extend({
    payButton: '.js-submit'
  });

  this.invoiceUrlRegExp = /bitpay\.com\/invoice\?id=(.+)$/;
};
utils.inherits(TweetViewPage, BasePage);

TweetViewPage.prototype.pay = function(params) {
  var self = this;
  this.casper.then(function() {
    this.click('.js-submit');
  });
  this.casper.waitForUrl(this.invoiceUrlRegExp);

  if (params) {
    var invoiceId;
    this.casper.then(function() {
      invoiceId = self.invoiceUrlRegExp.exec(this.getCurrentUrl())[1];
      this.test.assertSelectorHasText('header', 'activeqrowd.com');
      self.helpers.changeOrderStatus(invoiceId, params);
    });
  }
};
