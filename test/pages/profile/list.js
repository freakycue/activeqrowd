'use strict';

var BasePage = require('../base'),
  _ = require('underscore'),
  patchedRequire = patchRequire(require),
  utils = patchedRequire('utils');

var ProfileListPage = module.exports = function() {
  BasePage.apply(this, arguments);

  this.url = 'activists';

  this.elements = _(this.elements).extend({
    searchForm: 'form'
  });

  this.countersRegExp = new RegExp('(\\d+) tweets and (\\d+) cards in' +
    ' support of (\\d+) campaigns.');
};
utils.inherits(ProfileListPage, BasePage);

ProfileListPage.prototype.searchUsers = function(keywords) {
  var self = this;
  this.casper.then(function() {
    this.fill(self.elements.searchForm, {
      q: keywords
    }, true);
  });
  this.init();
};

ProfileListPage.prototype.getUserCounters = function(handle) {
  var countersText = this.casper.fetchText('[data-handle="' + handle + '"]');
  var parts = this.countersRegExp.exec(countersText);
  return {
    tweets: Number(parts[1]),
    cards: Number(parts[2]),
    campaigns: Number(parts[3])
  };
};
