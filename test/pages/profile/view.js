'use strict';

var BasePage = require('../base'),
  patchedRequire = patchRequire(require),
  utils = patchedRequire('utils'),
  _ = require('underscore');

var ProfileViewPage = module.exports = function() {
  BasePage.apply(this, arguments);
  this.url = 'activists/([A-Za-z0-9_]{1,15})';

  this.links = _(this.links).extend({
    'edit': '.js-edit-button'
  });
};
utils.inherits(ProfileViewPage, BasePage);

ProfileViewPage.prototype.getAddress = function() {
  return {
    name: this.casper.fetchText('.js-address-name'),
    address: this.casper.fetchText('.js-address-address'),
    additionalAddress: this.casper.fetchText('.js-address-additional-address'),
    city: this.casper.fetchText('.js-address-city'),
    state: this.casper.fetchText('.js-address-state'),
    zipCode: this.casper.fetchText('.js-address-zip-code')
  };
};
