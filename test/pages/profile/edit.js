'use strict';

var ProfileCompletePage = require('./complete'),
  _ = require('underscore'),
  patchedRequire = patchRequire(require),
  utils = patchedRequire('utils');

var ProfileEditPage = module.exports = function() {
  ProfileCompletePage.apply(this, arguments);
  this.url = 'activists/([A-Za-z0-9_]{1,15})';
};
utils.inherits(ProfileEditPage, ProfileCompletePage);

ProfileEditPage.prototype.getAddress = function() {
  var formAddress = this.casper.getFormValues(this.elements.addressForm),
    address = {};

  _(formAddress).each(function(val, key) {
    address[key.replace('address[', '').replace(']', '')] = val;
  });

  return address;
};
