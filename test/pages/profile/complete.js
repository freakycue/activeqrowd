'use strict';

var BasePage = require('../base'),
  patchedRequire = patchRequire(require),
  utils = patchedRequire('utils'),
  _ = require('underscore');

var ProfileCompletePage = module.exports = function() {
  BasePage.apply(this, arguments);
  this.url = 'profile/complete';
  this.elements = _(this.elements).extend({
    addressForm: 'form'
  });
};
utils.inherits(ProfileCompletePage, BasePage);

ProfileCompletePage.prototype.fillAddressForm = function(address, submit) {
  var self = this;
  this.casper.then(function() {
    this.fill(self.elements.addressForm, {
      'address[name]': address.name,
      'address[address]': address.address,
      'address[additionalAddress]': address.additionalAddress,
      'address[city]': address.city,
      'address[state]': address.state,
      'address[zipCode]': address.zipCode
    }, submit);
  });
};
