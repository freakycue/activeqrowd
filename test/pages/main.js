'use strict';

var BasePage = require('./base'),
  _ = require('underscore'),
  patchedRequire = patchRequire(require),
  utils = patchedRequire('utils');

var MainPage = module.exports = function() {
  BasePage.apply(this, arguments);
  this.url = '';
  this.elements = _(this.elements).extend({
    tweetPayButton: 'a[title="pay"]',
    tweetForm: '#tweet-form',
    tweetField: '[name="status"]',
    tweetLength: '.js-tweet-length',
    autoSuggestItem: '.textcomplete-item',
    popup: '.popover',
    popupContent: '.popover-content'
  });
};
utils.inherits(MainPage, BasePage);

MainPage.prototype.clickPay = function() {
  var self = this;
  this.casper.then(function() {
    this.click(self.elements.tweetPayButton);
  });
};

MainPage.prototype.init = function() {
  BasePage.prototype.init.apply(this, arguments);
  // this.casper.waitFor(function() {
  //   return this.fetchText(self.elements.tweetLength) === '133';
  // });
};

MainPage.prototype.type = function(msg) {
  var self = this;
  this.casper.then(function() {
    this.sendKeys(self.elements.tweetField, msg);
  });
};

MainPage.prototype.selectAutosuggestItem = function(type, index, callback) {
  var self = this;
  this.casper.waitForSelector(this.elements.autoSuggestItem, function() {
    var selector = self.elements.autoSuggestItem + '[data-index="' +
      index + '"]';
    var text = this.fetchText(selector),
      result;

    if (type === 'recipient') {
      var parts = /(@(.+))$/.exec(text);
      result = parts[1];
    } else {
      result = text;
    }

    this.click(selector);

    return callback(result);
  });
  this.casper.waitWhileVisible(this.elements.autoSuggestItem);
};

MainPage.prototype.getTweetText = function() {
  return this.helpers.getFieldContent(this.elements.tweetField);
};

MainPage.prototype.submitTweet = function() {
  var self = this;
  this.casper.then(function() {
    this.fill(self.elements.tweetForm, {}, true);
  });
};

MainPage.prototype.waitForPopup = function() {
  this.casper.waitUntilVisible(this.elements.popup);
};

MainPage.prototype.getPopupContent = function() {
  return this.casper.fetchText(this.elements.popupContent);
};
