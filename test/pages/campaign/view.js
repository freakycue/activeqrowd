'use strict';

var BasePage = require('../base'),
  _ = require('underscore'),
  patchedRequire = patchRequire(require),
  utils = patchedRequire('utils');

var CampaignViewPage = module.exports = function() {
  BasePage.apply(this, arguments);
  this.url = 'campaigns/(\\w+)';

  this.countersRegExp = new RegExp('^(\\d+) users supports campaign with' +
    ' (\\d+) tweets and (\\d+) cards.$');

  if (this.entity) {
    this.entity = this.entity.toLowerCase();
  }

  this.elements = _(this.elements).extend({
    counters: 'h3.grey',
    mentions: '.js-mentions'
  });
};
utils.inherits(CampaignViewPage, BasePage);

CampaignViewPage.prototype.getCounters = function() {
  var parts = this.countersRegExp.exec(this.casper
    .fetchText(this.elements.counters));
  return {
    users: Number(parts[1]),
    tweets: Number(parts[2]),
    cards: Number(parts[3])
  };
};

CampaignViewPage.prototype.getMentions = function() {
  return this.casper.fetchText(this.elements.mentions);
};
