'use strict';

var helpers = require('../helpers');

var BasePage = module.exports = function(entity) {
  this.entity = entity;
  /**
   * selectors of page elements
   */
  this.elements = {
    loginButton: '.js-login',
    logoutButton: '.js-logout'
  };

  this.links = {
    activist: '.js-profile',
    admin: '.js-admin',
    activists: '.js-activists'
  };
};

BasePage.prototype.casper = casper;
BasePage.prototype.helpers = helpers;

/**
 * navigate to page url
 */
BasePage.prototype.open = function(entity) {
  var url = this.getUrl(entity);
  this.casper.then(function() {
    this.echo('Load: ' + url + ' page', 'INFO');
  });
  this.casper.thenOpen(this.helpers.createUrl(url));
  this.init();
};

/**
 * wait for page load
 */
BasePage.prototype.init = function() {
  this.casper.waitForUrl(this.helpers.createMatchUrl(this.getUrl()));
};

/**
 * returns page url
 */
BasePage.prototype.getUrl = function(entity) {
  if (entity || this.entity) {
    return this.url.replace(/\((.+)\)/, entity || this.entity);
  }
  return this.url;
};

BasePage.prototype.getEntity = function() {
  if (!this.urlRegExp) {
    this.urlRegExp = new RegExp(this.url);
  }
  var parts = this.urlRegExp.exec(this.casper.getCurrentUrl());
  if (parts) {
    return parts[1];
  }
};

BasePage.prototype.navigateTo = function(link) {
  var self = this;
  this.casper.then(function() {
    this.echo('Navigate to: ' + link, 'INFO');
    this.click(self.links[link]);
  });
};

/**
 * login with current user
 */
BasePage.prototype.login = function(user) {
  var self = this;
  this.helpers.log('Login ' + user.email + ':' + user.password);
  this.casper.then(function() {
    this.click(self.elements.loginButton);
  });
  this.casper.waitForUrl(/twitter.com\/oauth\/authorize/, function() {
    var loginParams = {};
    if (this.exists('input[name="session[username_or_email]"]')) {
      loginParams['session[username_or_email]'] = user.email;
      loginParams['session[password]'] = user.password;
    }
    this.fill('#oauth_form', loginParams, true);
  });
  this.casper.waitForSelector(this.elements.logoutButton);
};

/**
 * logout
 */
BasePage.prototype.logout = function() {
  var self = this;
  this.casper.waitForSelector(this.elements.logoutButton, function() {
    this.echo('Logout', 'INFO');
    this.test.assertExists(self.elements.logoutButton);
    this.click(self.elements.logoutButton);
  });
  this.casper.waitForSelector(this.elements.loginButton);
};

BasePage.prototype.fetchText = function(baseSector, element) {
  var selector;
  if (arguments.length === 1) {
    selector = this.elements[baseSector];
  } else {
    selector = baseSector + ' ' + this.elements[element];
  }
  return this.casper.fetchText(selector);
};
