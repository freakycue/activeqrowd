'use strict';

var config = {};
config.development = {
  basePath: 'http://127.0.0.1:3000/',
  user: {
    email: 'freakycue+aq_test_bot@gmail.com',
    password: 'AQAndroid+44',
    handle: 'aq_test_bot',
    address: {
      name: 'Aleksey Timchenok',
      address: 'Baker St. 221B',
      city: 'London',
      state: 'London',
      zipCode: '12345'
    }
  },
  campaign: 'AQTest',
  adminCampaign: 'AdminAQTest',
  recipient: {
    name: 'Aleksey Timchenok',
    twitterHandle: 'aq_test_bot',
    address: {
      address: 'Baker St. 221B',
      city: 'London',
      state: 'London',
      zipCode: '12345'
    }
  }
};

module.exports = function(env) {
  return config[env || 'development'];
};
