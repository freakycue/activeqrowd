'use strict';

var twitter = require('../services/twitter'),
  _ = require('underscore'),
  config = require('../config')(),
  mongoose = require('mongoose'),
  Steppy = require('twostep').Steppy,
  EventEmitter = require('events').EventEmitter,
  util = require('util');

var User = mongoose.model('User'),
  Notification = mongoose.model('Notification'),
  Recipient = mongoose.model('Recipient'),
  Campaign = mongoose.model('Campaign'),
  Tweet = mongoose.model('Tweet');

var forbiddenHandles = {};
forbiddenHandles[config.twitter.testBotName] = 1;
forbiddenHandles[config.twitter.botName] = 1;

function TweetStream() {
  var self = this;
  EventEmitter.apply(this, arguments);

  this.stream = twitter.getStream('statuses/filter', {
    track: config.twitter.query
  });
  this.stream.on('tweet', function(tweet) {
    tweet = twitter.processTweet(tweet, true);

    if (tweet) {
      if (!forbiddenHandles[tweet.user.twitterHandle]) {
        // trigger tweet event for another listeners
        self.emit('tweet', tweet);
        // run handlers
        self.sendNotifications(tweet);
      }
      self.saveTweet(tweet);
      self.updateCampaign(tweet);
      self.updateProfile(tweet);
    }
  });
}
util.inherits(TweetStream, EventEmitter);
module.exports = new TweetStream();

TweetStream.prototype._errorHandler = function(err) {
  if (err) {
    console.log('[err]', err, err.stack);
  }
};

// save tweet object
TweetStream.prototype.saveTweet = function(tweet) {
  var tweetObj = new Tweet(_({}).extend({
    tweetId: tweet.id
  }, tweet));
  tweetObj.save(this._errorHandler);
};

// update user's statistics
TweetStream.prototype.updateProfile = function(tweet) {
  Steppy(
    function() {
      User.update({
        twitterHandle: tweet.user.twitterHandle
      }, {
        $inc: {
          'counters.tweets.count': 1
        }
      }, this.slot());

      var step = this;
      tweet.campaigns.forEach(function(campaign) {
        User.update({
          twitterHandle: tweet.user.twitterHandle,
          'counters.campaigns.items': {$ne: campaign}
        }, {
          $inc: {'counters.campaigns.count': 1},
          $addToSet: {'counters.campaigns.items': campaign}
        }, step.slot());
      });
    },
    this._errorHandler
  );
};

// inc tweets count for campaign
TweetStream.prototype.updateCampaign = function(tweet) {
  var campaigns = tweet.campaigns;

  if (tweet.entities.hashtags.length) {
    Steppy(
      function() {
        var step = this;
        var now = Date.now();
        campaigns.forEach(function(campaign) {
          // update tweets count
          Campaign.update({
            name: campaign
          }, {
            $inc: {
              'counters.tweets.count': 1
            },
            $setOnInsert: {
              isHidden: false,
              createDate: now,
              updateDate: now,
              _keywords: []
            }
          }, {upsert: true}, step.slot());
        });
      },
      function() {
        var step = this;

        campaigns.forEach(function(campaign) {
          // update users count
          Campaign.update({
            name: campaign,
            'counters.users.items': {$ne: tweet.user.twitterHandle}
          }, {
            $addToSet: {'counters.users.items': tweet.user.twitterHandle},
            $inc: {'counters.users.count': 1}
          }, step.slot());
        });
      },
      // newly created campaigns' keywords
      function() {
        Campaign.find({
          name: {$in: campaigns},
          _keywords: {$size: 0}
        }, this.slot());
      },
      function(err, campaigns) {
        var step = this;
        campaigns.forEach(function(campaign) {
          campaign.updateKeywords();
          campaign.save(step.slot());
        });
        this.pass(null);
      },
      this._errorHandler
    );
  }
};

TweetStream.prototype.sendNotifications = function(tweet) {
  Steppy(
    function() {
      User.findOne({twitterHandle: tweet.user.twitterHandle}, this.slot());
    },
    function(err, user) {
      if (!user) {
        this.pass('registrationRequired');
      } else {
        if (!user.isComplete) {
          // if user registered, but profile is incomplete
          this.pass('registrationCompleteRequired');
        } else {
          checkTweetRecipients(tweet, this.slot());
        }
      }
    },
    function(err, type) {
      if (type) {
        this.pass(type);
        // count notification with the same type for the last hour
        Notification.count({
          twitterHandle: tweet.user.twitterHandle,
          type: type,
          createDate: {$gt: Date.now() - (3600 * 1000)}
        }, this.slot());
      }
    },
    function(err, type, count) {
      if (!count) {
        var notification = Notification({
          twitterHandle: tweet.user.twitterHandle,
          inReplyTo: tweet.id,
          type: type
        });
        notification.save(this.slot());
      }
    },
    this._errorHandler
  );

  function checkTweetRecipients(tweet, callback) {
    var recipients = tweet.recipients;

    if (!recipients.length) {
      // if user doesn't add recipients
      callback(null, 'orderWithNoRecipients');
    } else {
      Steppy(
        function() {
          Recipient.count({
            twitterHandle: {$in: recipients}
          }, this.slot());
        },
        function(err, recipientsCount) {
          if (recipientsCount !== recipients.length) {
            // if some of recipients doesn't recognized
            this.pass('recipientsUnrecognized');
          } else {
            this.pass(null);
          }
        },
        callback
      );
    }
  }
};
