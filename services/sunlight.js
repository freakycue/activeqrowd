'use strict';

var request = require('request');


function Sunlight(config) {
  this.options = {
    apiKey: config.apiKey,
    basePath: config.basePath ||
      'https://congress.api.sunlightfoundation.com/'
  };
}
module.exports = Sunlight;

Sunlight.prototype.request = function(method, url, data, callback) {
  var options = {
    headers: {
      'X-APIKEY': this.options.apiKey
    }
  };

  options.method = method;
  options.url = this.options.basePath + url;

  if (method === 'GET') {
    options.url += data ? '?' + serialize(data) : '';
  } else {
    options.form = data;
  }

  request(options, function(err, resp, body) {
    if (err) return callback(err);

    var result = JSON.parse(body);
    if (result.error) callback(result.error);
    else callback(null, result);
  });
};

Sunlight.prototype.get = function(url, params, callback) {
  this.request('GET', url, params, callback);
};

Sunlight.prototype.post = function(url, params, callback) {
  this.request('POST', url, params, callback);
};

function serialize(obj) {
  var str = [];
  for (var p in obj) {
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
    }
  }
  return str.join('&');
}
