'use strict';

var Bitly = require('bitly'),
  config = require('../config')();


function BitlyService(config) {
  this.bitly = new Bitly(config.username, config.apiKey);
}
module.exports = new BitlyService(config.bitly);

BitlyService.prototype.request = function(method, params, callback) {
  this.bitly[method](params, function(err, response) {
    if (response.status_code !== 200) {
      var error = new Error(response.status_txt);
      error.status_code = response.status_code;
      return callback(error);
    }
    callback(err, response);
  });
};

BitlyService.prototype.shorten = function(url, callback) {
  this.request('shorten', url, function(err, response) {
    if (err) return callback(err);
    callback(null, response.data.url);
  });
};
