'use strict';

var Twit = require('twit'),
  Steppy = require('twostep').Steppy,
  mongoose = require('mongoose'),
  _ = require('underscore'),
  errors = require('../utils/errors'),
  config = require('../config')(),
  OAuth = require('oauth').OAuth;

var User = mongoose.model('User'),
  Order = mongoose.model('Order'),
  Tweet = mongoose.model('Tweet'),
  TwitterCache = mongoose.model('TwitterCache');

var oauth = new OAuth(
  'https://api.twitter.com/oauth/request_token',
  'https://api.twitter.com/oauth/access_token',
  config.twitter.consumerKey,
  config.twitter.consumerSecret,
  '1.0A',
  config.basePath + 'auth/twitter/callback',
  'HMAC-SHA1'
);

module.exports = new Twitter(config.twitter);

function Twitter(options) {
  this.query = options.query;

  this.twit = new Twit({
    consumer_key: options.consumerKey,
    consumer_secret: options.consumerSecret,
    access_token: options.token,
    access_token_secret: options.tokenSecret
  });
}

Twitter.prototype._errorHandler = function(callback) {
  return function(err, args) {
    if (err && err.statusCode) {
      console.log('[twitter service] error:', err.statusCode, err);
      if (err.data) {
        var errData = JSON.parse(err.data);
        err.code = errData.errors[0].code;
        err.message = errData.errors[0].message;
      }
      err.status = err.statusCode;
    }
    callback(err, args);
  };
};

Twitter.prototype.cacheUrls = {
  'search/tweets': 180 * 1000, // 3 mins cache
  'statuses/show': 1800 * 1000
};

Twitter.prototype._request = function(method, url, params, callback) {
  var self = this,
    key = method + JSON.stringify(params),
    cacheInterval = self.cacheUrls[url];

  Steppy(
    function() {
      if (cacheInterval) {
        // if url is cached - try to get it
        TwitterCache.findOne({
          key: key,
          expires: {$gt: Date.now()}
        }, {response: 1}, {noError: true}, this.slot());
      } else this.pass(null);
    },
    function(err, cache) {
      if (cache) {
        callback(null, cache.response);
      } else {
        self.twit[method](url, params, this.slot());
      }
    },
    function(twitterErr, data) {

      if (twitterErr && cacheInterval) {
        // if error happens, log error and try to find cache
        console.log('[twitter service] error:', twitterErr, twitterErr.stack);
        TwitterCache.findOne({
          key: key
        }, {response: 1}, function(err, cache) {
          if (err) return callback(err);
          // if cache doesn't exists - throw special error
          if (!cache) return callback(
            new errors.TwitterServiceError(
              twitterErr.message,
              twitterErr.statusCode)
            );

          callback(null, cache.response);
        });
      } else {
        callback(twitterErr, data);

        if (!twitterErr && cacheInterval) {
          // save results into cache collection
          TwitterCache.update({
            key: key
          }, {
            $set: {
              response: data,
              // 30 mins uptime
              expires: Date.now() + cacheInterval,
              ttl: new Date()
            }
          }, {upsert: true}, function(err) {
            if (err) console.log('[twitter service] error:', err, err.stack);
          });
        }
      }
    }
  );
};

Twitter.prototype.search = function(query, options, callback) {
  var self = this;

  if (!callback) {
    callback = options;
    options = {};
  } else {
    options = options || {};
  }

  Steppy(
    function() {
      self._request('get', 'search/tweets', _(options).defaults({
        q: self.query + (query ? ' ' + query : ''),
        count: 100
      }), this.slot());
    },
    function(err, data) {
      this.pass(_(data.statuses.map(function(tweet) {
        return self.processTweet(tweet);
      })).compact());
    },
    this._errorHandler(callback)
  );
};

Twitter.prototype.getLastTweet = function (callback) {
  var self = this;
  Steppy(
    function() {
      Tweet.find({
        createDate: {$gt: Date.now() - self.cacheUrls['search/tweets']}
      }, null, {limit: 1}).sort({createDate: -1}).exec(this.slot());
    },
    function(err, tweets) {
      if (tweets.length) {
        callback(null, tweets[0]);
      } else {
        self.search(null, {count: 1}, this.slot());
      }
    },
    function(err, tweets) {
      this.pass(tweets.length && tweets[0]);
    },
    callback
  );
};

Twitter.prototype.searchByCampaign = function(campaign, options, callback) {
  var self = this;

  if (!callback) {
    callback = options;
  }
  options = options || {};

  Steppy(
    function() {
      self.search('#' + campaign, options, this.slot());
      Tweet.find({
        campaigns: campaign,
        createDate: {$gt: Date.now() - self.cacheUrls['search/tweets']}
      }, null, {limit: options.count}, this.slot());
    },
    function(err, tweets, cachedTweets) {
      tweets = _(cachedTweets.concat(tweets)).uniq(function(tweet) {
        return tweet.id;
      });
      this.pass(tweets);
    },
    callback
  );
};

Twitter.prototype.get = function(id, callback) {
  var self = this;
  Steppy(
    function() {
      self._request('get', 'statuses/show', {id: id}, this.slot());
    },
    function(err, data) {
      this.pass(self.processTweet(data, true));
    },
    this._errorHandler(callback)
  );
};

Twitter.prototype.getUser = function(id, callback) {
  var self = this;
  Steppy(
    function() {
      self._request('get', 'users/show', {id: id}, this.slot());
    },
    this._errorHandler(callback)
  );
};

/**
 * find and sync users with twitter API
 */
Twitter.prototype.findUsers = function(query, fields, options, callback) {
  var self = this;

  if (!callback) {
    callback = options;
  }
  options = options || {};

  Steppy(
    function() {
      var cursor = User.find(query, fields, options, this.slot());
      if (options.sort) {
        cursor.sort(options.sort);
      }
      cursor.exec(this.slot());
    },
    function(err, users) {
      this.pass(users);
      if (users.length) {
        self._request('get', 'users/lookup', {
          user_id: _(users).pluck('twitterId'),
          include_entities: false
        }, this.slot());
      } else {
        this.pass([]);
      }
    },
    function(err, users, twitterUsers) {
      var usersHash = _(twitterUsers).indexBy('id_str'),
        step = this;
      this.pass(users);

      _(users).forEach(function(user) {
        var profile = usersHash[user.twitterId];
        if ((user.twitterHandle !== profile.screen_name) ||
          (user.displayName !== profile.name) ||
          (user.mainImage !== profile.profile_image_url_https)) {
          user.set('twitterHandle', profile.screen_name);
          user.set('displayName', profile.name);
          user.set('mainImage', profile.profile_image_url_https);
          user.save(step.slot());
        }
      });
    },
    function(err, users) {
      this.pass(users);
    },
    callback
  );
};

/**
 * find and sync orders with Twitter API
 */
Twitter.prototype.findOrders = function(query, fields, options, callback) {
  var self = this;

  if (!callback) {
    callback = options;
  }
  options = options || {};

  Steppy(
    function() {
      var cursor = Order.find(query, fields, options);
      if (options.sort) {
        cursor.sort(options.sort);
      }
      cursor.exec(this.slot());
    },
    function(err, orders) {
      var tweetIds = _(orders).chain().map(function(order) {
        return order.tweet && order.tweet.id;
      }).compact().value();
      this.pass(orders);
      if (tweetIds.length) {
        var group = this.makeGroup();
        tweetIds.forEach(function(id) {
          var cb = group.slot();
          self.get(id, function(err, tweet) {
            if (err && (err.status === 404)) {
              cb();
            } else {
              cb(err, tweet);
            }
          });
        });
      }
    },
    function(err, orders, tweets) {
      var tweetHash = _(tweets).chain().compact().indexBy('id').value(),
        step = this;

      this.pass(orders);
      orders.forEach(function(order) {
        if (!order.isDeleted) {
          var tweet = tweetHash[order.tweet.id];
          if (tweet) {
            order.set('tweet', tweet);
          } else {
            order.set('isDeleted', true);
          }
          order.save(step.slot());
        }
      });
    },
    function(err, orders) {
      this.pass(orders);
    },
    callback
  );
};

Twitter.prototype.send = function(tweet, options, callback) {
  var self = this;

  if (!callback) {
    callback = options;
    options = {};
  } else {
    options = options || {};
  }

  Steppy(
    function() {
      oauth.post(
        'https://api.twitter.com/1.1/statuses/update.json',
        options.token || config.twitter.token,
        options.tokenSecret || config.twitter.tokenSecret,
        tweet,
        this.slot());
    },
    function(err, tweet) {
      this.pass(self.processTweet(JSON.parse(tweet), true));
    },
    this._errorHandler(callback)
  );
};

Twitter.prototype.makeTweetUrl = function(tweet) {
  return 'https://twitter.com/' + tweet.user.screen_name + '/status/' +
    tweet.id_str;
};

Twitter.prototype.processTweet = function(tweet, noStrict) {
  // filter tweets from test bot
  if (!noStrict && (tweet.user.screen_name === config.twitter.testBotName)) {
    return;
  }

  var recipients = _(tweet.entities.user_mentions).pluck('screen_name');
  recipients = _(recipients).filter(function(name) {
    return name !== config.twitter.botName;
  });

  return {
    createDate: new Date(tweet.created_at).getTime(),
    id: tweet.id_str,
    text: tweet.text,
    user: {
      twitterId: tweet.user.id_str,
      twitterHandle: tweet.user.screen_name,
      displayName: tweet.user.name,
      avatar: tweet.user.profile_image_url_https
    },
    recipients: recipients,
    campaigns: _(tweet.entities.hashtags).chain().pluck('text')
    .map(function(campaign) {
      return campaign.toLowerCase();
    }).value(),
    entities: tweet.entities,
    url: this.makeTweetUrl(tweet)
  };
};


Twitter.prototype.getStream = function(url, params) {
  var stream = this.twit.stream(url, params);

  // bind to stream events
  stream.on('disconnect', function(disconnectMessage) {
    console.log('[twitter service] stream ', url,
      ' was disconnected with message: ', disconnectMessage);
  });
  stream.on('warning', function(warning) {
    console.log('[twitter service] stream warning: ', warning);
  });
  stream.on('connect', function() {
    console.log('[twitter service] stream ', url, ' was connected.');
  });

  return stream;
};
