'use strict';

var request = require('request'),
  config = require('../config')();


function BitPay(config) {
  this.options = {
    apiKey: config.apiKey,
    basePath: config.basePath || 'https://bitpay.com/api/'
  };
}
module.exports = new BitPay(config.bitpay);

BitPay.prototype.request = function(method, url, data, callback) {
  var options = {
    auth: {
      user: this.options.apiKey,
      sendImmediately: true
    }
  };

  options.method = method;
  options.url = this.options.basePath + url;

  if (method === 'GET') {
    options.url += data ? '?' + serialize(data) : '';
  } else {
    options.form = data;
  }

  request(options, function(err, resp, body) {
    if (err) return callback(err);
    var result = JSON.parse(body);
    if (result.error) callback(result.error);
    else callback(null, result);
  });
};

BitPay.prototype.invoiceCreate = function(invoice, callback) {
  this.request('POST', 'invoice', invoice, callback);
};

BitPay.prototype.invoiceGet = function(invoiceId, callback) {
  this.request('GET', 'invoice/' + invoiceId, null, callback);
};

function serialize(obj) {
  var str = [];
  for (var p in obj) {
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
    }
  }
  return str.join('&');
}
