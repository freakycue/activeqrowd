'use strict';

var mongoose = require('mongoose'),
  helpers = require('../utils/helpers');

var Static = mongoose.model('Static');

var urlRegExp = new RegExp('^/(' + helpers.editablePages.join('|') + ')');
module.exports = function(req, res, next) {
  var parts,
    path = req._parsedUrl.pathname;

  if (path !== '/') {
    parts = urlRegExp.exec(path);
  } else {
    parts = [null, 'main'];
  }

  if (parts) {
    Static.findOne({
      key: parts[1]
    }, function(err, pageContent) {
      if (err) return next(err);

      req.pageStaticContent = pageContent;
      next();
    });
  } else {
    next();
  }
};
