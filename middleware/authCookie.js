'use strict';

/**
 * Mark authenticated user with cookie for outside caching
 */
var cookieName = 'activecrowd-authenticated',
  cookieOptions = {httpOnly: true};

module.exports = function(req, res, next) {
  var cookieValue = req.cookies[cookieName];
  if (req.user && req.user._id) {
    if (!cookieValue) res.cookie(cookieName, 'true', cookieOptions);
  } else {
    if (cookieValue) res.clearCookie(cookieName, cookieOptions);
  }
  next();
};
