'use strict';

module.exports = function(req, res, next) {
  var user = req.user;

  if (!user.isComplete && !(/\/profile\/complete/).test(req.url)) {
    req.session.returnTo = req.url;
    res.redirect('/profile/complete');
  } else {
    next();
  }
};
