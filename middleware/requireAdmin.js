'use strict';

var errors = require('../utils/errors');

module.exports = function(req, res, next) {
  if (!req.user.isAdmin) {
    return next(new errors.ForbiddenError());
  }
  next();
};
