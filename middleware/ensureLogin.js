'use strict';

var ensureLogin = require('connect-ensure-login').ensureLoggedIn,
  errors = require('../utils/errors');

module.exports = function(req, res, next) {
  if (req.method === 'get') {
    // standart flow for get requests
    ensureLogin('/auth/login')(req, res, next);
  } else {
    if (req.user && req.user.id) {
      next();
    } else {
      // forbidden error throw for unauthorized users
      next(new errors.ForbiddenError());
    }
  }
};
