'use strict';

var mongoose = require('mongoose');

var Campaign = mongoose.model('Campaign');

module.exports = function(req, res, next) {
  var campaignName = req.param('campaign');
  if (campaignName !== campaignName.toLowerCase()) {
    return res.redirect(301, '/campaigns/' + campaignName.toLowerCase());
  }
  Campaign.findOne({name: campaignName}, function(err, campaign) {
    if (err) return next(err);

    req.campaign = campaign;
    next();
  });
};
