'use strict';

var errors = require('../utils/errors'),
  is = require('type-is');

/*jshint unused:false */
module.exports = function(err, req, res, next) {
  // log error
  console.log('[err] ', err, err.stack);

  // set response status
  res.status(err.status || 500);

  if (is(req, ['application/json'])) {
    console.log('JSONHandler');
    JSONHandler(res, err);
  } else {
    HTMLHandler(res, err);
  }
};

function HTMLHandler(res, err) {
  // render error page
  res.render('error/error', {err: {
    status: err.status || 500,
    message: err.status ? err.message : 'Internal Server Error'
  }});
}

function JSONHandler(res, err) {
  res.json({
    status: err.status || 500,
    name: err.name || 'InternalServerError',
    message: err.message || 'Internal server error.',
    code: err.code
  });
}
