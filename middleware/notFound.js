'use strict';

module.exports = function(req, res) {
  res.status(404);
  res.render('error/error', {
    err: {
      status: 404,
      message: 'Page not found.'
    }
  });
};
