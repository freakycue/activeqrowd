'use strict';

var gulp = require('gulp'),
  cache = require('gulp-cache'),
  _ = require('underscore'),
  tap = require('gulp-tap'),
  shell = require('gulp-shell');


/**
 * lint tasks
 */
gulp.task('jslint', function() {
  var jshint = require('gulp-jshint');
  require('jshint-stylish');

  gulp.src([
    '**/*.js',
    '!public/scripts/**',
    '!node_modules/**',
    '!public/js/lib/**'
  ]).pipe(jshint())
  .pipe(jshint.reporter('jshint-stylish'))
  .pipe(jshint.reporter('fail'));
});

gulp.task('csslint', function() {
  var csslint = require('gulp-csslint');

  gulp.src(['public/css/*.css'])
  .pipe(csslint('public/css/.csslintrc'), {
    success: function(file) {
      return !file.csslint.errorCount;
    },
    value: function(file) {
      return {
        csslint: file.csslint
      };
    }
  })
  .pipe(csslint.reporter())
  .pipe(csslint.reporter(function(file) {
    if (file.csslint.errorCount) {
      throw new Error();
    }
  }));
});

gulp.task('lint', ['jslint', 'csslint']);

/**
 * client-side optimizations
 */
gulp.task('minifyJS', function(cb) {
  var rjs = require('requirejs'),
    uglify = require('gulp-uglify'),
    names = [];

  gulp.src(['public/js/app/**/*.js']).pipe(tap(function(file) {
    names.push(file.path.replace(file.base, ''));
  })).on('end', function() {
    var config = require('./config/require')();
    config.paths.ckeditor = 'empty:';

    rjs.optimize(_(config).extend({
      appDir: 'public/js/',
      baseUrl: './',
      dir: 'public/scripts/',
      logLevel: 0,
      optimize: 'none',
      modules: names.map(function(name) {
        return {
          name: 'app/' + name.replace('.js', '')
        };
      })
    }), function() {
      gulp.src(['public/scripts/**/*.js'])
      .pipe(tap(function(file) {
        console.log('uglify2: ', file.path.replace(file.base, ''));
      }))
      .pipe(cache(uglify()))
      .pipe(gulp.dest('public/scripts/'))
      .on('end', function() {
        cb();
      });
    }, function(err) {
      console.log('[err]', err, err.stack);
      cb(err);
    });
  });
});

gulp.task('minifyCSS', function() {
  var minify = require('gulp-minify-css');

  gulp.src([
    'public/css/*.css'
  ])
  .pipe(minify({
    relativeTo: 'public/css',
    processImport: true
  }))
  .pipe(gulp.dest('public/styles/'));

  // copy fonts
  gulp.src(['public/css/fonts/*'])
  .pipe(gulp.dest('public/styles/fonts'));
});
gulp.task('build', ['minifyJS', 'minifyCSS']);

/**
 * database tasks
 */
var lastBackupDate = '2014_07_14_05-00';
gulp.task('restoreAndMigrate', shell.task([
  [
    'mkdir backup',
    'scp root@activeqrowd.com:/root/backup/' + lastBackupDate +
      '.tar.gz' +
      ' ./backup/' + lastBackupDate + '.tar.gz',
    'tar -xzf ./backup/' + lastBackupDate + '.tar.gz -C ./backup/',
    'mongorestore --drop ./backup/' + lastBackupDate,
    'rm -r ./backup',
    'east migrate'
  ].join(' && ')
]));

gulp.task('clearGulpCache', function(cb) {
  cache.clearAll(cb);
});
