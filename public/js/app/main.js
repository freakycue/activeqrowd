'use strict';

require(['socket.io', 'ckeditor', 'jquery', 'app/layout', 'jqueryTextComplete',
  'app/static'],
function(io, ckeditor, $) {
  $(function() {
    var io = window.io;
    var socket = io.connect();
    var $lastTweetContainer = $('.js-last-tweet');

    socket.on('tweet', function(tweet) {
      var $lastTweet = $lastTweetContainer.find('.tweet'),
        lastTweet = $lastTweet.data('tweet');

      if (lastTweet && lastTweet.campaigns && lastTweet.campaigns.length) {
        var query = [];
        $(lastTweet.campaigns).each(function() {
          query.push('[data-campaign-name=' + this + ']');
        });
        query = query.join(', ');

        var $campaign = $(query);
        if ($campaign.length) {
          $($campaign[0]).prepend($lastTweet);
        }
      }

      $lastTweetContainer.html(tweet.view);
    });

    var $status = $('[name="status"]');
    if ($status.length) {

      var recipientsReq,
        campaignsReq;

      $status.textcomplete([{
        match: /(^|\s)@([A-Za-z0-9_]{1,15})$/,
        search: function(term, callback) {
          if (recipientsReq) {
            recipientsReq.abort();
          }

          recipientsReq = $.post('/recipients/search', {
            query: term
          }, function(res) {
            callback(res);
          }, 'json');

          callback([]);
        },
        replace: function(recipient) {
          return '$1@' + recipient.twitterHandle + ' ';
        },
        template: function(recipient) {
          return '<span>' + recipient.name + ' @' + recipient.twitterHandle +
            '</span>';
        },
        maxCount: 15,
        cache: true
      }, {
        match: /(^|\s)#(\w+)$/,
        search: function(term, callback) {
          if (campaignsReq) {
            campaignsReq.abort();
          }

          campaignsReq = $.post('/campaigns/search', {
            query: term
          }, function(res) {
            callback(res);
          }, 'json');

          callback([]);
        },
        replace: function(campaign) {
          return '$1#' + campaign.name + ' ';
        },
        template: function(campaign) {
          return '<span>' + campaign.name + '</span>';
        },
        maxCount: 15,
        cache: true
      }]);

      var tweetMaxLength = 140,
        botName = '@qrdbt',
        cursorPosition = botName.length + 1;

      var $tweetLength = $('.js-tweet-length'),
        resetTextArea = function($el) {
          var campaign = $el.data('campaign');
          $el.focus().val(botName + ' ' + (campaign ? ' #' + campaign : ''));
          $el.get(0).setSelectionRange(cursorPosition, cursorPosition);
          $tweetLength.text(tweetMaxLength - $status.val().length);
        };

      resetTextArea($status);
      $status.on('keydown', function() {
        var left = tweetMaxLength - $status.val().length;
        $tweetLength.text(left);
        if (left > 10) {
          $tweetLength.removeClass('text-danger');
        } else {
          $tweetLength.addClass('text-danger');
        }
      });
      $status.on('click focus', function() {
        $status.popover('hide');
        $status.popover('destroy');
      });

      var showHint = function(content) {
        $status.popover('destroy');
        $status.popover({
          placement: 'top',
          trigger: 'manual',
          content: content,
          html: true
        });
        $status.popover('show');
      };

      var tweetReq, hashtagWarning;
      $('#tweet-form').on('submit', function() {
        var status = $status.val(),
          recipients = status.match(/@(?!qrdbt)[A-Za-z0-9_]{1,15}/g) || [],
          campaigns = status.match(/#(\w+)/g) || [];

        if (!/@qrdbt/.test(status)) {
          showHint('Don\'t forget to mention @qrdbt in your tweet.');
          return false;
        }

        if (!recipients.length) {
          showHint('Add at least one recipient.');
          return false;
        }

        if (!campaigns.length) {
          if (!hashtagWarning) {
            hashtagWarning = true;
            showHint('<span>We recommend a #campaign hashtag so your' +
              ' Tweet can be searched for more easily.</span><br>' +
              '<small>Click \'Submit\' to ignore this message and send' +
              ' without a #campaign hashtag.</small>');
            return false;
          }
        }

        if (tweetReq) tweetReq.abort();

        tweetReq = $.ajax({
          type: 'post',
          url: '/tweets/add',
          contentType: 'application/json',
          data: JSON.stringify({status: status}),
          dataType: 'json',
          success: function(tweet) {
            resetTextArea($status);
            window.location.href = '/tweets/' + tweet.id;
          }
        }).fail(function(res) {
          if (res.status !== 404) {
            var error = JSON.parse(res.responseText);

            if (error.code === 187) {
              showHint('Whoops! You already tweeted that...');
            } else {
              showHint('Something goes wrong... Our specialists already' +
                ' working on that problem.');
            }
          }
        });

        return false;
      });
    }
  });
});
