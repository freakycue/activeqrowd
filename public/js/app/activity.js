'use strict';

require(['socket.io', 'jquery', 'app/layout', 'app/static'],
function(io, $) {
  $(function() {
    var socket = io.connect();
    var $timeline = $('.timeline'),
      query = $('input[name="q"]').data('query');

    if (query) {
      var queryRegExp = new RegExp('(' +
        query.replace(/\W/g, '.').split('.').join('|') + ')');
    }

    socket.on('tweet', function(tweet) {
      if (query) {
        if (queryRegExp.test(tweet.text)) {
          $timeline.prepend(tweet.view);
        }
      } else {
        $timeline.prepend(tweet.view);
      }
    });
  });
});
