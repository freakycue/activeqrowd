'use strict';

require(['app/lib/editor', 'jquery', 'app/layout'], function(Editor, $) {
  $(function() {
    new Editor();
  });
});
