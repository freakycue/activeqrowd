'use strict';

define(['jquery', 'ckeditor', 'bootstrap'], function($) {
  var Editor = function() {
    var self = this;

    this.editor = window.CKEDITOR;

    this.editor.disableAutoLine = true;

    this.form = document.getElementById('save-form');
    $('.js-save-changes').on('click', function() {
      self.save();
    });
    $(this.form).on('submit', function() {
      self.save();
    });

    this.editor.config.format_h1 = {
      element: 'h1',
      attributes: {'class': 'grey text-center'}
    };

    this.editor.stylesSet.add([{
      name: 'Grey Heading 1',
      element: 'h1',
      attributes: {'class': 'grey text-center'}
    }]);

    this.editor.plugins.add('save', {
      init: function(editor) {
        editor.addCommand('saveMetadata', {
          exec: function() {
            $('#saveDialog').modal('toggle');
            $('#cke_editable').slideToggle();
          }
        });

        editor.ui.addButton('MetadataForm', {
          label: 'Save',
          command: 'saveMetadata',
          toolbar: 'document'
        });
      }
    });

    this.editor.on('instanceReady', function() {
      document.querySelector('.cke_button__metadataform_icon').innerHTML = '✔';
    });

    this.editor.config.extraPlugins = 'save';

    this.pageName = window.location.pathname === '/' ?
      '/main' : window.location.pathname;
  };

  Editor.prototype.save = function() {
    if (!this.form.checkValidity()) {
      return;
    }

    var content = this.editor.instances.editable.getData(),
      title = $('[name="title"]').val(),
      description = $('textarea[name="description"]').val();

    if (this.saveReq) {
      this.saveReq.abort();
    }

    this.saveReq = $.ajax({
      type: 'post',
      url: '/admin/static' + this.pageName,
      contentType: 'application/json',
      data: JSON.stringify({
        title: title,
        description: description,
        content: content
      }),
      dataType: 'json',
      success: function() {
        // hide modal
        $('#saveDialog').modal('toggle');
      }
    });
  };

  return Editor;
});
