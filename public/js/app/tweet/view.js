'use strict';

require(['jquery', 'app/layout'], function($) {
  $(function() {
    var $price = $('.js-price'),
      price = $price.data('price'),
      $recipients = $('select[name="recipients[]"]'),
      $submitButton = $('.js-submit');

    $recipients.on('change', function() {
      var recipientsCount = $recipients.find('option:selected').length;
      $price.text(price * recipientsCount);
      if (recipientsCount) {
        $submitButton.removeClass('disabled');
      } else {
        $submitButton.addClass('disabled');
      }
    }).trigger('change');
  });
});
