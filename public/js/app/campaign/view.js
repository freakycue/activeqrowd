'use strict';

require(['socket.io', 'jquery', 'app/layout'], function(io, $) {
  $(function() {
    var socket = io.connect();
    var $timeline = $('.timeline'),
      campaign = $('input[name="campaign"]').val();

    socket.on('tweet', function(tweet) {
      if (tweet.campaigns.length) {
        for (var i = 0; i < tweet.campaigns.length; i++) {
          if (tweet.campaigns[i] === campaign) {
            $timeline.prepend(tweet.view);
            return;
          }
        }
      }
    });
  });
});
