'use strict';

var response = require('express').response;

var redirectOriginal = response.redirect;
response.redirect = function(url) {
  var status = 302;
  // allow status / url
  if (arguments.length === 2) {
    if ('number' === typeof url) {
      status = url;
      url = arguments[1];
    } else {
      status = arguments[1];
    }
  }

  if (!/^\/auth/.test(url) && !/^http/.test(url)) {
    url = [this.req.baseUrl.replace(/\/$/, ''), url.replace(/^\//, '')]
      .join('/');
  }

  redirectOriginal.call(this, status, url);
};
