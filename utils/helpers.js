'use strict';

var Steppy = require('twostep').Steppy,
  _ = require('underscore');

/**
 * base method for page with search function
 */
exports.search = function(params) {
  var limit = params.limit || 30,
    model = params.model,
    fields = params.fields;

  return function(query, options, callback) {
    Steppy(
      function() {
        options.skip = limit * (options.page - 1);
        options.limit = limit;

        if (query) {
          model.search(query, fields, options, this.slot());
        } else {
          model.count(options.conditions, this.slot());
          var cursor = model.find(options.conditions, fields, _(options)
          .omit('populate'));
          if (options.sort) {
            cursor.sort(options.sort);
          }
          if (options.populate) {
            options.populate.forEach(function(obj) {
              cursor.populate(obj.path, obj.fields);
            });
          }
          cursor.exec(this.slot());
        }
      },
      function(err, data, items) {
        if (items) {
          this.pass({
            results: items,
            totalCount: data
          });
        } else {
          this.pass(data);
        }
      },
      callback
    );
  };
};

exports.staticPages = [
  'about',
  'gettingstarted',
  'faq',
  'support',
  'contact',
  'tos'
];

exports.editablePages = exports.staticPages.concat([
  'activists',
  'activity',
  'campaigns'
]);
