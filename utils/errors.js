'use strict';

var inherits = require('util').inherits;

/**
 * Module contains all application error's classes
 * Errors handles by errorHandler middleware
 */

/**
 * Base class for all errors.
 * If a string is passed to the first param, it will be used as error's message.
 */
var BaseError = exports.BaseError = function(message, status) {
  Error.captureStackTrace(this, this);
  this.name = 'InternalServerError';
  this.message = message || 'Internal Server Error';
  this.status = status || 500;
};
inherits(BaseError, Error);

/**
 * Not found error
 */
var NotFoundError = exports.NotFoundError = function(message) {
  this.name = 'NotFoundError';
  this.message = message || 'Not Found';
  this.status = 404;
};
inherits(NotFoundError, BaseError);

/**
 * Forbidden error
 */
var ForbiddenError = exports.ForbiddenError = function(message) {
  this.name = 'ForbiddenError';
  this.message = message || 'Forbidden';
  this.status = 403;
};
inherits(ForbiddenError, BaseError);

/**
 * Twitter service error
 */
var TwitterServiceError = exports.TwitterServiceError =
function(message) {
  this.name = 'TwitterServiceError';
  message = message || 'Twitter service is temporary unavailable.';
  BaseError.apply(this, arguments);
};
inherits(TwitterServiceError, BaseError);

/**
 * Validation error
 */
var ValidationError = exports.ValidationError = function(message) {
  this.name = 'ValidationError';
  message = message || 'Validation error.';
  BaseError.apply(this.arguments);
};
inherits(ValidationError, BaseError);
