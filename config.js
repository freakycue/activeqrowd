'use strict';

var configBuilder = require('deep-conf')();

configBuilder.register({
  name: 'development',
  config: {
    basePath: 'http://127.0.0.1:3000/',
    staticUrlArgs: 'v=' + require('./package.json').version,
    db: 'mongodb://localhost:27017/activeqrowd',
    twitter: {
      consumerKey: 'Bn1QTWfvew7y1D1qx50lp7lsm',
      consumerSecret: 'YNhFc1hCeUVobtigaCZuomszsz1plgid4s9p1qn1Jap2x2ZoH5',
      botName: 'qrdbt',
      testBotName: 'aq_test_bot',
      token: '2310172212-adLGmvVtWih5pm951n5cfVjvTeixrZGlmwWSdw9',
      tokenSecret: 'FpjtVaHItLFi6V8mcAhiwpj8rUY8WVamUAQIYyQIrdOoz',
      query: '@qrdbt'
    },
    bitpay: {
      apiKey: 'QF3bMeRolHtIgmryjE9v9bCtUqq0FnUyhSdUGiGpg',
      // minimal price for bitpay
      price: 0.0001
    },
    bitly: {
      username: 'freakycue',
      apiKey: 'R_0c1e8abfcec94574a33432fcba45d33c'
    },
    sunlight: {
      apiKey: '42c8bbfdcfd647f9bc5354687caf8bbc'
    },
    mandrill: {
      apiKey: 'sehA8hcchFo4ZlWdDa_Tmw',
      email: 'freakycue@gmail.com',
      name: 'Pavel Vlasov'
    },
    port: 3000,
    secret: '7e0c5b36bec8f7b8970285466f53994c',
    useTestingRoutes: true,
    useOptimizedScripts: true,
    useMinCss: false,
    useAnalytics: false
  }
});

configBuilder.register({
  name: 'production',
  parent: 'development',
  config: {
    basePath: 'https://activeqrowd.com/',
    bitpay: {price: 0.0013},
    twitter: {
      // @qrdbt
      token: '2433761210-YQdO799VbFXsYhvXZLOJLj8evN5KQdnZCN0h1tH',
      tokenSecret: 'eWZVuwyadC3mGIwtSZz6hhV1cfZQM3n9L1lJtyKPhtFJo'
    },
    mandrill: {
      email: 'dev@activeqrowd.com',
      name: 'ActiveQrowd Dev'
    },
    useTestingRoutes: false,
    useOptimizedScripts: true,
    useMinCss: true,
    useAnalytics: true
  }
});

module.exports = function(env) {
  return configBuilder.get(env || process.env.NODE_ENV || 'development');
};
