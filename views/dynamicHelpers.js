'use strict';

var configBuilder = require('../config'),
  config = configBuilder();

module.exports = exports;

exports.currentUser = function(req) {
  return req.user;
};

exports.config = function() {
  return configBuilder();
};

exports.pageStaticContent = function(req) {
  return req.pageStaticContent;
};

exports.currentUrl = function(req) {
  return config.basePath.replace(/\/$/, '') + req.url;
};
