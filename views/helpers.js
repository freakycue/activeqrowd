'use strict';

var config = require('../config')(),
  moment = require('moment'),
  _ = require('underscore'),
  twitterTxt = require('twitter-text');

module.exports = function(hbs) {

  var entityTypeHash = {
    tweet: {
      path: 'tweets'
    },
    campaign: {
      path: 'campaigns',
      key: 'name'
    },
    profile: {
      path: 'activists',
      key: 'twitterHandle'
    },
    order: {
      path: 'orders'
    }
  };
  hbs.registerHelper('href', function(type, entity, action, options) {
    if (type in entityTypeHash) {
      var entityType = entityTypeHash[type];
      if (!options) {
        options = action;
        action = '';
      }
      return href.apply(this, [
        entityType.path,
        entity[entityType.key || 'id'],
        action,
        options
      ]);
    }
    return href.apply(this, arguments);
  });

  function href() {
    var args = makeArray(arguments),
      parts = _(args.slice(0, -1)).compact(),
      options = args[args.length - 1],
      query = _(options.hash).map(function(val, key) {
        return val ? key + '=' + encodeURIComponent(val) : key;
      }).join('&');

    return config.basePath + parts.join('/') + (query && ('?' + query));
  }

  hbs.registerHelper('formatDate', function(date, format) {
    format = _(format).isString() ? format : 'MMM, D YYYY';
    return moment(date).format(format);
  });

  hbs.registerHelper('JSONStringify', function(data) {
    return JSON.stringify(data);
  });
  hbs.registerHelper('JSONPrettify', function(data) {
    return JSON.stringify(data, null, '\t');
  });

  hbs.registerHelper('join', function(array, string) {
    return array && array.join(string);
  });

  hbs.registerHelper('paginate', require('handlebars-paginate'));

  hbs.registerHelper('getFormattedTweetText', function(tweet) {
    return twitterTxt.autoLinkWithJSON(tweet.text, tweet.entities, {
      target: '_blank'
    });
  });

  hbs.registerHelper('getRequireJSConfig', function() {
    return JSON.stringify(require('../config/require')(), null, '\t');
  });

  hbs.registerHelper('getKey', function(obj, path, defaultVal) {
    var parts = path.split('.'),
      res = obj;

    for(var i = 0, key; key = parts[i], key; i++) {
      if (!res[key]) return defaultVal;
      res = res[key];
    }
    return res;
  });

  function makeArray(obj) {
    return Array.prototype.slice.call(obj, 0, obj.length);
  }
};
