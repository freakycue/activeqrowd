'use strict';

exports.migrate = function(client, done) {
	var db = client.db;
	
  db.collection('statics').insert([{
    key: 'main',
    content: '<h1 class="grey text-center">Start a new campaign via' +
      ' Twitter #activeqrowd</h1>',
    title: 'Start a new campaign via Twitter #activeqrowd',
    description: 'Start a new campaign via Twitter #activeqrowd'
  }, {
    key: 'activity',
    content: '<h1 class="grey text-center">Latest #activeqrowd activity</h1>',
    title: 'Latest #activeqrowd activity',
    description: 'Latest #activeqrowd activity'
  }, {
    key: 'campaigns',
    content: '<h1 class="grey text-center">Hottest campaigns on' +
      ' #activeqrowd</h1>',
    title: 'Hottest #activeqrowd campaigns',
    description: 'Hottest #activeqrowd campaigns'
  }, {
    key: 'activists',
    content: '<h1 class="grey text-center">Top activists on #activeqrowd</h1>',
    title: 'Top #activeqrowd activists',
    description: 'Top #activeqrowd activists'
  }], done);
};
