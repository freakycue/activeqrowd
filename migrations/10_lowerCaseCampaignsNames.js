'use strict';

var Steppy = require('twostep').Steppy;

exports.migrate = function(client, done) {
	var db = client.db,
    campaignsCol = db.collection('campaigns');

  Steppy(
    function() {
      campaignsCol.find().toArray(this.slot());
    },
    function(err, campaigns) {
      var step = this;
      campaigns.forEach(function(campaign) {
        campaignsCol.update({_id: campaign._id}, {
          $set: {name: campaign.name.toLowerCase()}
        }, step.slot());
      });
    },
    done
  );
};
