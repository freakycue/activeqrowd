'use strict';

var Steppy = require('twostep').Steppy;

exports.migrate = function(client, done) {
	var db = client.db,
    cols = {
      users: db.collection('users'),
      campaigns: db.collection('campaigns'),
      notifications: db.collection('notifications'),
      orders: db.collection('orders'),
      recipients: db.collection('recipients'),
      reports: db.collection('reports'),
      twittercaches: db.collection('twittercaches')
    };
	
  Steppy(
    function() {
      // users
      cols.users.ensureIndex({twitterId: 1}, {unique: 1}, this.slot());
      cols.users.ensureIndex({twitterHandle: 1}, {unique: 1}, this.slot());
      cols.users.ensureIndex({createDate: -1}, this.slot());
      cols.users.ensureIndex({
        _keywords: 1,
        createDate: 1
      }, this.slot());
      cols.users.ensureIndex({
        isAdmin: 1,
        isComplete: 1,
        createDate: -1
      }, this.slot());

      // campaigns
      cols.campaigns.ensureIndex({name: 1}, {unique: 1}, this.slot());
      cols.campaigns.ensureIndex({
        'counters.tweets.count': -1
      }, this.slot());
      cols.campaigns.ensureIndex({
        name: 1,
        'counters.users.items': 1
      }, this.slot());

      // notifications
      cols.notifications.ensureIndex({
        isProcessed: 1,
        createDate: 1
      }, this.slot());

      // orders
      cols.orders.ensureIndex({
        invoiceId: 1,
        hash: 1
      }, {unique: true}, this.slot());
      cols.orders.ensureIndex({user: 1, createDate: -1}, this.slot());
      cols.orders.ensureIndex({status: 1, isProcessed: 1}, this.slot());

      // recipients
      cols.recipients.ensureIndex({name: 1}, this.slot());
      cols.recipients.ensureIndex({
        twitterHandle: 1
      }, {unique: true}, this.slot());
      cols.recipients.ensureIndex({_keywords: 1, name: 1}, this.slot());

      // reports
      cols.reports.ensureIndex({createDate: -1}, this.slot());

      // twittercaches
      cols.twittercaches.ensureIndex({key: 1}, this.slot());
      cols.twittercaches.ensureIndex({
        ttl: 1
      }, {expireAfterSeconds: 3600}, this.slot());
    },
    done
  );
};
