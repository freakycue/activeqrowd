'use strict';

var Steppy = require('twostep').Steppy;

exports.migrate = function(client, done) {
	var db = client.db,
    cols = {
      tweets: db.collection('tweets'),
      users: db.collection('users')
    };

  Steppy(
    function() {
      // tweets
      cols.tweets.ensureIndex({tweetId: 1}, {unique: true}, this.slot());
      cols.tweets.ensureIndex({'user.twitterId': 1}, this.slot());
      cols.tweets.ensureIndex({campaigns: 1}, this.slot());

      // users
      cols.users.ensureIndex({
        twitterHandle: 1,
        'counters.campaigns.items': 1
      }, this.slot());
    },
    done
  );
};
