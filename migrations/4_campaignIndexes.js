'use strict';

var mongoose = require('mongoose'),
  Steppy = require('twostep').Steppy;

require('../models/campaign');

exports.migrate = function(client, done) {
	var db = client.db;
	
  Steppy(
    function() {
      mongoose.connect(db.options.url);

      db.collection('campaigns').ensureIndex({isHidden: 1}, this.slot());

      var Campaign = mongoose.model('Campaign');
      Campaign.setKeywords(this.slot());
    },
    function() {
      mongoose.disconnect();
      this.pass(null);
    },
    done
  );
};
