'use strict';

exports.migrate = function(client, done) {
	var db = client.db;
	
  db.collection('campaigns').update({}, {
    $set: {isHidden: false}
  }, {multi: true}, done);
};
