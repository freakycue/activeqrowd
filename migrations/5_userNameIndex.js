'use strict';

exports.migrate = function(client, done) {
	var db = client.db;
	
  db.collection('users').ensureIndex({name: 1}, done);
};
