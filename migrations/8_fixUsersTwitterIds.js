'use strict';

var Steppy = require('twostep').Steppy;

exports.migrate = function(client, done) {
	var db = client.db,
    usersCol = db.collection('users');
	
  Steppy(
    function() {
      usersCol.find({}, {twitterId: 1}).toArray(this.slot());
    },
    function(err, users) {
      var step = this;
      users.forEach(function(user) {
        usersCol.update({
          twitterId: user.twitterId
        }, {
          $set: {twitterId: user.twitterId.toString()}
        }, step.slot());
      });
    },
    done
  );
};
