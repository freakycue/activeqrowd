'use strict';

var Steppy = require('twostep').Steppy;

exports.migrate = function(client, done) {
	var db = client.db,
    statics = db.collection('statics');
	
  Steppy(
    function() {
      statics.ensureIndex({key: 1}, {unique: true}, this.slot());
      statics.insert([{
        key: 'about',
        content: '<h1>About ActiveQrowd</h1>'
      }, {
        key: 'gettingstarted',
        content: '<h1>Getting started</h1>'
      }, {
        key: 'faq',
        content: '<h1>FAQ</h1>'
      }, {
        key: 'support',
        content: '<h1>Support</h1>'
      }, {
        key: 'contact',
        content: '<h1>Contact us</h1>'
      }, {
        key: 'tos',
        content: '<h1>Terms of Service</h1>'
      }], this.slot());
    },
    done
  );
};
