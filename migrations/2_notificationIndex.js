'use strict';

exports.migrate = function(client, done) {
	var db = client.db;
	
  db.collection('notifications').ensureIndex({
    twitterHandle: 1,
    type: 1,
    createDate: 1
  }, done);
};
